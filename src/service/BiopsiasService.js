import Axios from "axios";

export default class BiopsiasService {
    getBiopsias() {
        return Axios.get('/biopsias')
            .then(res => res.data);
    }
}



import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Rating } from 'primereact/rating';
import { ProductService } from '../../service/ProductService';
import { InputNumber } from 'primereact/inputnumber';
import { searchCodigosFonasa, searchBiopsiaCodigoFonasa, saveBiopsiaCodigoFonasa, postCodigoFonasaBiopsia } from './CodigosFonasaService';

const TablaCodigoFonasa = ({ idBiopsia }) => {
    const [products, setProducts] = useState([]);
    const productService = new ProductService();
    const [CodigosFonasaBiopsia, setCodigosFonasaBiopsia] = useState([]);

    useEffect(() => {
        searchCod();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps



    const searchCod = async () => {
        let result = await searchCodigosFonasa();
        let id = idBiopsia;
        console.log(id);

        let codFonasa = await searchBiopsiaCodigoFonasa(id);
        if (!codFonasa) {
            for (var x in result) {
                let data = {
                    "biopsiaId": id,
                    "codigo": result[x].codigo,
                    "cantidad": 0
                };
                let pos = await postCodigoFonasaBiopsia(data);
                console.log(pos);
                CodigosFonasaBiopsia.push({
                    id: result[x].id,
                    codigo: result[x].codigo,
                    descripcion: result[x].descripcion,
                    cantidad: 0,
                    biopsiaId: id
                })
            }

        } else {
            for (var z in result) {
                for (var b in codFonasa) {
                    if (result[z].codigo === codFonasa[b].codigo) {
                        CodigosFonasaBiopsia.push({
                            id: result[z].id,
                            codigo: result[z].codigo,
                            descripcion: result[z].descripcion,
                            cantidad: codFonasa[b].cantidad,
                            biopsiaId: id
                        })

                    }


                }
            }
        }


        // if (codFonasa) {
        //     for (var x in result) {
        //         for (var y in codFonasa) {
        //             if (result[x].codigo === codFonasa[y].codigo) {
        //                 CodigosFonasaBiopsia.push({
        //                     id: result[x].id,
        //                     codigo: result[x].codigo,
        //                     descripcion: result[x].descripcion,
        //                     cantidad: codFonasa[y].cantidad,
        //                     biopsiaId: id
        //                 })
        //             }
        //         }
        //     }
        // } else if (codFonasaResponse) {
        //     for (var z in result) {
        //         for (var b in codFonasa) {
        //             if (result[z].codigo === codFonasa[b].codigo) {
        //                 CodigosFonasaBiopsia.push({
        //                     id: result[x].id,
        //                     codigo: result[x].codigo,
        //                     descripcion: result[x].descripcion,
        //                     cantidad: codFonasa[y].cantidad,
        //                     biopsiaId: id
        //                 })
        //             }
        //         }
        //     }
        // }

        console.log(CodigosFonasaBiopsia);
        setProducts(CodigosFonasaBiopsia);



    };
    const onRowEditComplete1 = (e) => {
        let _products = [...products];
        let { newData, index } = e;
        console.log(e);
        console.log(e.data);
        _products[index] = newData;
        let codigosBiopsia = _products[index];
        let id = idBiopsia;
        let codigo = codigosBiopsia.codigo;
        let data = {
            "biopsiaId": id,
            "codigo": codigo,
            "cantidad": codigosBiopsia.cantidad
        };
        console.log(data);
        saveBiopsiaCodigoFonasa(id, data, codigo);
        console.log(_products[index]);
        setProducts(_products);
    }
    const CantCodigosEditor = (options) => {
        console.log(options.value);
        return <InputNumber value={options.value} onValueChange={(e) => options.editorCallback(e.value)} />
    }

    return (
        <div>

            <div className="card">
                <DataTable value={products} editMode="row" dataKey="id" onRowEditComplete={onRowEditComplete1} header="Stack" responsiveLayout="stack" breakpoint="960px">
                    <Column field="descripcion" header="Descripción" />
                    <Column field="codigo" header="Código" />
                    <Column field="cantidad" header="Cantidad" editor={(options) => CantCodigosEditor(options)} />
                    <Column rowEditor headerStyle={{ width: '10%', minWidth: '8rem' }} bodyStyle={{ textAlign: 'center' }}></Column>
                </DataTable>
            </div>
        </div>
    );
}
export default TablaCodigoFonasa;

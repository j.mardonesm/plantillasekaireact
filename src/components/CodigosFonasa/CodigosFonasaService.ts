import axios from 'axios';



export async function searchCodigosFonasa() {
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-codigo-fonasa/api/fonasa`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }

}


export async function searchBiopsiaCodigoFonasa(id: string) {
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-codigo-fonasa/api/codfonasa/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function saveBiopsiaCodigoFonasa(id: string, data: {}, codigo: string) {
    try {
        const headers = {
            'Content-Type': 'application/json'
        }
        const response = await axios.put(`http://10.69.206.32:8080/api-apa-codigo-fonasa/api/codfonasa/${id}/${codigo}`, data,
            { headers });
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function postCodigoFonasaBiopsia(data: {}){
    try {
        const response = await axios.post(`http://10.69.206.32:8080/api-apa-codigo-fonasa/api/codfonasa`, data);
        console.log(data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}


import React, { useState, useEffect, useRef } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import FormBuscarBiopsia from "./FormBuscarBiopsia";
import { useForm, Controller } from "react-hook-form";
import { classNames } from 'primereact/utils';
import { TabView, TabPanel } from 'primereact/tabview';
import { searchPersonaByRut, searchNroFichaById, searchByNroBiopsia, searchBiopsiaByIdPaciente } from './RegistroBiobsiaService'
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Toast } from 'primereact/toast';
import { Suspense, lazy } from 'react';
import FormularioBiopsia from "./FormularioBiopsia";
import NroBiopsia from "./NroBiopsia";
import Swal from "sweetalert2";
import TableBiopsiasPaciente from "./TableBiopsiasPaciente";
import { OverlayPanel } from "primereact/overlaypanel";

const BusquedaBiopsias = () => {
    const [pacienteRut, setPacienteRut] = useState("");
    const [loading1, setLoading1] = useState(false);
    const [loading2, setLoading2] = useState(false);
    const [loading3, setLoading3] = useState(false);
    const [formaBuscarBiop, setFormaBuscarBiop] = useState(false);
    const [TableBiopsias, setTableBiopsias] = useState(false);
    const [desplegarTabla, setDesplegarTabla] = useState(false);
    const [activeIndex, setActiveIndex] = useState(0);
    const [nroBiopsia, setNroBiopsia] = useState(false);
    const [desplegarTablaCrear, setDesplegarTablaCrear] = useState(false);
    const [desplegarBusBiopsia, setDesplegarBusBiopsia] = useState(false);
    const [dataPersonas, setDataPersonas] = useState([]);
    const [BiopsiasPaciente, setBiopsiasPaciente] = useState([]);
    const [displayResponsive, setDisplayResponsive] = useState(false);
    const op = useRef(null);



    useEffect(() => {

    }, []);

    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();

    const submitRut = async (data, e) => {
        console.log(data);
        console.log(String(data.pacRut));
        let run = data.pacRut;
        let indice = run.indexOf('-');
        let rut = run.substring(0, indice);
        console.log(rut);

        //setPacienteRut(data.pacRut);

        let result = null;
        if (ValidarRut.validaRut(data.pacRut)) {
            result = await searchPersonaByRut(rut);
            console.log(result);
            setPacienteRut(data.pacRut);
            reset({
                "pacRut": ""
            });
            if (result) {
                await Object.assign(dataPersonas, result);
                console.log(dataPersonas);
                setDesplegarTabla(!desplegarTabla && hiddenBuscarPaciente);
                onLoadingClick3();
            } else {
                successAlert();


            }

        } else {
            getFormErrorMessage(data.pacRut)

        }
    };
    const toast = useRef(null);
    const reject = () => {
        toast.current.show({ severity: 'warn', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
    }
    const successAlert = () => {
        Swal.fire({
            title: 'Alerta!',
            text: 'Este Rut No Cumple con el Formato o no está Registrado',
            icon: 'warning',
        });
    };
    const TableBiopsiasAlert = () => {
        Swal.fire({
            title: 'Alerta!',
            text: 'El Paciente no Registra Biopsias',
            icon: 'warning',
        });
    };
    const biopsiaAlert = () => {
        Swal.fire({
            title: 'Biopsia No Encontrada!',
            text: 'La biopsia no existe o no fue registrada!',
            icon: 'warning',
        });
    };




    const submitNroFicha = (data, e) => {
        console.log(data);
        reset();
        //setDesplegarTabla(!desplegarTabla && hiddenBuscarPaciente);
    };
    const submitNrBiopsia = async (data, e) => {
        let result = await searchByNroBiopsia(data.numBiopsia);
        if (result) {
            setNroBiopsia(data.numBiopsia);
            setFormaBuscarBiop(!formaBuscarBiop);
            setDesplegarBusBiopsia(!desplegarBusBiopsia);
        } else {
            biopsiaAlert();
        }
        console.log(data);
        reset();

        //setDesplegarTabla(!desplegarTabla && hiddenBuscarPaciente);
    };
    const submitBiopsiasPorPaciente = async (data, e) => {
        console.log(String(data.RutPaciente));
        let run = data.RutPaciente;
        let indice = run.indexOf('-');
        let rut = run.substring(0, indice);
        console.log(rut);
        let result = null;
        let biopsiasPaciente
        if (ValidarRut.validaRut(data.RutPaciente)) {
            result = await searchPersonaByRut(rut);
            console.log(result);
            setPacienteRut(data.pacRut);
            reset({
                "pacRut": ""
            });
            if (result) {
                // await Object.assign(dataPersonas, result);
                // console.log(dataPersonas);
                onLoadingClick3();
                biopsiasPaciente = await searchBiopsiaByIdPaciente(result[0].id);
                if (biopsiasPaciente) {
                    setTableBiopsias(!TableBiopsias);
                    setDesplegarBusBiopsia(!desplegarBusBiopsia);
                } else {
                    return TableBiopsiasAlert();
                }
            } else {
                return successAlert();


            }

        } else {
            return getFormErrorMessage(data.RutPaciente)

        }

        console.log(biopsiasPaciente);
        let FinalTable = [];
        biopsiasPaciente.map((e, index) => {
            let TableBiop;
            if(e.informeprocesado){
                TableBiop = {
                    id : index + 1,
                    nrobiopsia: e.nrobiopsia,
                    NombrePaciente: result[0].primerNombre + " " + result[0].apellidoPaterno + " " + result[0].apellidoMaterno,
                    fecharecepcionmuestra: e.fecharecepcionmuestra,
                    fechatomamuestra: e.fechatomamuestra,
                    fechainforme : e.fechainforme,
                    muestraenviada: e.muestraenviada,
                    antecedentes: e.antecedentes,
                    idBiopsia : e.id,
                    organo1 : e.organo1,
                    organo2: e.organo2,
                };

                FinalTable = [...FinalTable,TableBiop];
            }


        });

        console.log(FinalTable);
        setBiopsiasPaciente(FinalTable);
        // if(result){
        //     setNroBiopsia(data.numBiopsia);
        //     setFormaBuscarBiop(!formaBuscarBiop);
        //     setDesplegarBusBiopsia(!desplegarBusBiopsia);
        // }else{
        //     biopsiaAlert();
        // }
        console.log(result);
        reset();

        //setDesplegarTabla(!desplegarTabla && hiddenBuscarPaciente);
    };
    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };
    const ValidarRut = {
        // Valida el rut con su cadena completa "XXXXXXXX-X"
        validaRut: function (rutCompleto) {
            rutCompleto = rutCompleto.replace("‐", "-");
            if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
                return false;
            var tmp = rutCompleto.split('-');
            var digv = tmp[1];
            var rut = tmp[0];
            if (digv == 'K') digv = 'k';

            return (ValidarRut.dv(rut) == digv);
        },
        dv: function (T) {
            var M = 0, S = 1;
            for (; T; T = Math.floor(T / 10))
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
            return S ? S - 1 : 'k';
        }
    };
    const hiddenBuscarPaciente = async () => {
        await setDesplegarTablaCrear(false);
        return;
    };
    const onLoadingClick1 = async () => {
        setLoading1(true);
        //desplegarTabla(true);

        setTimeout(() => {
            setLoading1(false);
        }, 2000);
    };
    const onLoadingClick2 = async () => {
        setLoading2(true);
        //desplegarTabla(true);

        setTimeout(() => {
            setLoading2(false);
        }, 2000);
    };
    const onLoadingClick3 = async () => {
        setLoading3(true);
        //desplegarTabla(true);

        setTimeout(() => {
            setLoading3(false);
        }, 2000);
    };

    const dropdownItems = [
        {
            id: 1,
            name: "Numero Biopsia",
        },
        {
            id: 2,
            name: "Rut",
        },
        {
            id: 3,
            name: "Numero de Ficha",
        },
    ];

    return (
        <div className="p-grid">

            <div className="col-12 md:col-12">
                <div className="card">
                    <h4>Biopsias</h4>
                    <p>Cree una nueva Biopsia o busque una ya existente</p>
                    <div className="p-fluid formgrid grid">
                        <hr />
                        <div className="" hidden={desplegarBusBiopsia}>
                            <span className="p-float-label">
                                <Button id="addBiopsia" type="submit" className="mr-2 mb-2" icon="pi pi-plus" loading={loading1} onClick={() => setDesplegarTablaCrear(!desplegarTablaCrear && onLoadingClick1)} disabled={desplegarTablaCrear || desplegarTabla || formaBuscarBiop || TableBiopsias} />
                            </span>
                        </div>
                        <div className="" hidden={desplegarTablaCrear}>
                            <Button type="submit" className="mr-2 mb-2" icon="pi pi-search" loading={loading2} onClick={() => setDesplegarBusBiopsia(true && onLoadingClick2)} disabled={desplegarBusBiopsia || desplegarTabla || formaBuscarBiop || TableBiopsias} />
                        </div>
                        <div hidden={!desplegarBusBiopsia || formaBuscarBiop}>
                            <Button icon="pi pi-times" className="p-button-danger mr-2 mb-2" slot="end" onClick={() => setDesplegarBusBiopsia(!desplegarBusBiopsia)} disabled={!desplegarBusBiopsia} />
                        </div>
                        <div hidden={!desplegarTablaCrear}>
                            <Button icon="pi pi-times" className="p-button-danger mr-2 mb-2" slot="end" onClick={() => setDesplegarTablaCrear(!desplegarTablaCrear)} disabled={!desplegarTablaCrear} />
                        </div>
                        <div hidden={!desplegarTabla}>
                            <Button icon="pi pi-times" className="p-button-danger mr-2 mb-2" slot="end" onClick={() => setDesplegarTabla(!desplegarTabla)} />
                        </div>
                        <div hidden={!formaBuscarBiop}>
                            <Button icon="pi pi-times" className="p-button-danger mr-2 mb-2" slot="end" onClick={() => setFormaBuscarBiop(!formaBuscarBiop)} />
                        </div>
                        <div hidden={!TableBiopsias}>
                            <Button icon="pi pi-times" className="p-button-danger mr-2 mb-2" slot="end" onClick={() => setTableBiopsias(!TableBiopsias)} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={desplegarTablaCrear ? null : "show-element"}>
                {desplegarTablaCrear && (
                    <div className="p-grid">
                        <div className="col-12 md:col-12">
                            <div className="card">
                                <div className="card-header">
                                    <div className="card-title">
                                        <h3>Buscar Paciente</h3>
                                        <label>Seleccione una opción de busqueda</label>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <hr />
                                    <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                                        <TabPanel header="Rut Paciente">
                                            <Toast ref={toast} />
                                            <ConfirmDialog />
                                            <div className="field col-12 md:col-5">
                                                <form onSubmit={handleSubmit(submitRut)}>
                                                    <br />
                                                    <br />
                                                    <div className="p-inputgroup">
                                                        <span className="p-float-label">
                                                            <Controller name="pacRut" control={control} rules={{ required: "El rut debe tener un formato valido. Ej 12345678-9" }} render={({ field, fieldState }) =>
                                                                <InputText id={field.name} value={field.value} {...field} autoFocus className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                                            <label htmlFor="pacRut" className={classNames({ 'p-error': errors.name })}>Ingrese Rut</label>
                                                            {/* <InputText id="PacienteRut"  name="pacRut" type="text" maxLength={10}
                                                            {...register("pacRut", { required: true, maxLength: 10 })} /> */}
                                                        </span>
                                                        <Button id="pacRutBtn" className="p-button-primary" icon="pi pi-search" loading={loading3} />
                                                    </div>
                                                    {getFormErrorMessage('pacRut')}

                                                    {/* <span className="p-error">{errors?.pacRut?.type === "required" && <span className="p-error">El Rut es requerido</span>}</span> */}
                                                </form>
                                            </div>
                                        </TabPanel>
                                        <TabPanel header="Nro Ficha Paciente">
                                            <div className="field col-12 md:col-5">
                                                <form onSubmit={handleSubmit(submitNroFicha)}>

                                                    <br />
                                                    <br />
                                                    <div className="p-inputgroup">
                                                        <span className="p-float-label">
                                                            <Controller name="pacNroFicha" control={control} rules={{ required: "Debe Ingresar el Número de Ficha" }} render={({ field, fieldState }) =>
                                                                <InputText id={field.name} {...field} autoFocus className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                                            <label htmlFor="pacNroFicha" className={classNames({ 'p-error': errors.name })}>Ingrese Número de Ficha</label>
                                                        </span>
                                                        <Button id="pacNroFichaBtn" className="p-button-primary" icon="pi pi-search" loading={loading2} />
                                                    </div>
                                                    {getFormErrorMessage('pacNroFicha')}
                                                </form>
                                            </div>
                                        </TabPanel>
                                    </TabView>

                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            <div className={desplegarBusBiopsia ? null : "show-element"}>
                {desplegarBusBiopsia && (
                    <div className="p-grid">
                        <div className="col-12 md:col-12">
                            <div className="card">
                                <div className="card-header">
                                    <div className="card-title">
                                        <h3>Buscar Biopsia</h3>
                                        <label>La Busqueda debe ser por el numero de Biopsia, si desea un  historico, busque biopsias por paciente</label>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <hr />
                                    <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                                        <TabPanel header="Número de Biopsia">
                                            <Toast ref={toast} />
                                            <ConfirmDialog />
                                            {/* <Dialog header="Header" visible={displayResponsive} style={{ width: '25vw' }} footer={renderFooter('displayResponsive')} onHide={() => onHide('displayResponsive')}>

                                            </Dialog> */}
                                            <div className="field col-12 md:col-5">
                                                <form onSubmit={handleSubmit(submitNrBiopsia)}>

                                                    <br />
                                                    <br />
                                                    <div className="p-inputgroup">
                                                        <span className="p-float-label">
                                                            <Controller name="numBiopsia" control={control} rules={{ required: "El campo es Obligatorio" }} render={({ field, fieldState }) =>
                                                                <InputText id={field.name} {...field} autoFocus autoComplete="off" className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                                            <label htmlFor="numBiopsia" className={classNames({ 'p-error': errors.name })}>Ingrese Biopsia</label>
                                                            {/* <InputText id="PacienteRut"  name="pacRut" type="text" maxLength={10}
                                                            {...register("pacRut", { required: true, maxLength: 10 })} /> */}
                                                        </span>
                                                        <Button id="numBiopsiaBtn" className="p-button-primary" icon="pi pi-search" loading={loading3} />
                                                    </div>
                                                    {getFormErrorMessage('numBiopsia')}

                                                    {/* <span className="p-error">{errors?.pacRut?.type === "required" && <span className="p-error">El Rut es requerido</span>}</span> */}
                                                </form>
                                            </div>
                                        </TabPanel>
                                        <TabPanel header="Biopsias Por Paciente">
                                            <Toast ref={toast} />
                                            <ConfirmDialog />
                                            <div className="field col-12 md:col-5">
                                                <form onSubmit={handleSubmit(submitBiopsiasPorPaciente)}>

                                                    <br />
                                                    <br />
                                                    <div className="p-inputgroup">
                                                        <span className="p-float-label">
                                                            <Controller name="RutPaciente" control={control} rules={{ required: "El rut debe tener un formato valido. Ej 12345678-9" }} render={({ field, fieldState }) =>
                                                                <InputText id={field.name} {...field} autoFocus className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                                            <label htmlFor="RutPaciente" className={classNames({ 'p-error': errors.name })}>Ingrese Rut Paciente</label>
                                                            {/* <InputText id="PacienteRut"  name="pacRut" type="text" maxLength={10}
                                                            {...register("pacRut", { required: true, maxLength: 10 })} /> */}
                                                        </span>
                                                        <Button id="numBiopsiaBtn" className="p-button-primary" icon="pi pi-search" loading={loading3} />
                                                    </div>
                                                    {getFormErrorMessage('RutPaciente')}

                                                    {/* <span className="p-error">{errors?.pacRut?.type === "required" && <span className="p-error">El Rut es requerido</span>}</span> */}
                                                </form>
                                            </div>
                                        </TabPanel>
                                    </TabView>

                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>

            <div className={desplegarTabla ? null : "show-element"}>
                {desplegarTabla && (
                    <div className="col-12 md:col-12">
                        <NroBiopsia InfoPersona={dataPersonas} />
                    </div>
                )}
            </div>
            <div className={formaBuscarBiop ? "show-element" : null}>
                {formaBuscarBiop && (
                    <div className="col-12 md:col-12">
                        <FormBuscarBiopsia nroBiopsia={nroBiopsia} />
                    </div>
                )}
            </div>
            <div className={TableBiopsias ? "show-element" : null}>
                {TableBiopsias && (
                    <div className="col-12 md:col-12">
                        <TableBiopsiasPaciente biopsiasPaciente={BiopsiasPaciente} />
                    </div>
                )}
            </div>
        </div>
    );
};
const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};
export default React.memo(BusquedaBiopsias, comparisonFn);

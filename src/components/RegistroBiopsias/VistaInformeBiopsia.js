import React, { useState, useEffect, useRef } from "react";
import logoHCSBA from "../../assets/img/logoHCSBA.jpg";
import { searchDiagnosticosById } from "../InformeBiopsia/InformeBiopsiaService";
import { searchMacroMicro } from "../MacroMicro/MacroMicroService";
import { listOrganos } from "./RegistroBiobsiaService";

const VistaInformeBiopsia = ({ biopsiasPaciente }) => {
    const [imagenHCSBA, setImagenHCSBA] = useState(logoHCSBA);
    const [InformeBiopsia, setInformeBiopsia] = useState({});
    const [base64, setBase64] = useState();
    useEffect(() => {
        search();
    }, []);
    const InformeModel = {
        micro: "",
        macro: "",
        diagnostico: "",
        nota: "",
        nBiopsia: "",
        organo : "",
    };
    function toDataURL(src, callback) {
        var image = new Image();
        image.crossOrigin = 'Anonymous';

        image.onload = function () {
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            canvas.height = this.naturalHeight;
            canvas.width = this.naturalWidth;
            context.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL('image/jpeg');
            callback(dataURL);
        };
        image.src = src;
    }
    const search = async () => {
        console.log(biopsiasPaciente);
        InformeModel.macro = biopsiasPaciente.macro;
        InformeModel.micro = biopsiasPaciente.micro;
        InformeModel.diagnostico = biopsiasPaciente.diag;
        InformeModel.nota = biopsiasPaciente.nota;
        InformeModel.nBiopsia = biopsiasPaciente.nrobiopsia;
        InformeModel.organo = biopsiasPaciente.organo;
        setInformeBiopsia({ ...InformeModel });
        console.log(InformeModel);





    };
    return (
        <React.Fragment>
            <div className="p-fluid formgrid grid flex justify-content-start">
                <div className="flex col-12 md:col-12">

                    <h6><strong><h4>Biopsia : </h4></strong>{InformeBiopsia.nBiopsia}</h6>
                </div>
                <div className="flex col-12 md:col-12">
                    <h6>
                    <strong><h4>Organo : </h4></strong>{InformeBiopsia.organo}
                    </h6>
                </div>
                <div className="flex col-12 md:col-12 justify-content-start">

                    <h6 ><strong><h4>Macroscopia : </h4></strong>{InformeBiopsia.macro}</h6>
                </div>
                <div className="flex col-12 md:col-12 justify-content-start">

                    <h6 ><strong><h4>Microscopia : </h4></strong>{InformeBiopsia.micro}</h6>
                </div>
                <div className="flex col-12 md:col-12 justify-content-start">

                    <h6 ><strong><h4>Diagnostico : </h4></strong>{InformeBiopsia.diagnostico}</h6>
                </div>
                <div className="flex col-12 md:col-12 justify-content-start">

                    <h6 ><strong><h4> Nota : </h4></strong>{InformeBiopsia.nota}</h6>
                </div>
            </div>
        </React.Fragment>
    )
}
export default VistaInformeBiopsia;

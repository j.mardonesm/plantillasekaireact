export default interface Biopsia {
    id?: string;
    nrobiopsia?: string;
    auge?: string;
    personaId?: string;
    medicotratante?: string;
    fechatomamuestra?: string;
    fecharecepcionmuestra?: string;
    servicio?: string;
    organo1?: string;
    organo2?: string;
    patologo?: string;
    tecnologo?: string;
    dias?: string;
    fechainforme?: string;
    fechaentregainforme?: string;
    biopsia?: string;
    biopsiarapida?: string;
    citologico?: string;
    necropsia?: string;
    placas?: string;
    muestraenviada?: string;
    antecedentes?: string;
    tipoinformeid?: string;
    formatomuestraid?: string;
}

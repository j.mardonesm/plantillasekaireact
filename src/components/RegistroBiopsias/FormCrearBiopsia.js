import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dropdown } from 'primereact/dropdown';
import { ListaDeBiopsias } from './ListaBiopsias';
import { InputSwitch } from 'primereact/inputswitch';
import { ToggleButton } from 'primereact/togglebutton';
import { InputNumber } from 'primereact/inputnumber';
import { Calendar } from 'primereact/calendar';
import { Image } from "primereact/image";


import { locale, addLocale, updateLocaleOption, updateLocaleOptions, localeOption, localeOptions } from 'primereact/api';

addLocale('es', {
    firstDayOfWeek: 1,
    dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
    dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    today: 'Hoy',
    clear: 'Limpiar'
});

locale('es');





const FormCrearBiopsias = (props) => {

    const [dropdownItem, setDropdownItem] = useState(null);
    const [serviciosdeOrigen, setServiciosdeOrigen] = useState(null);
    const [presentacionMuestra, setPresentacionMuestra] = useState(null);
    const [cantMuestra, setCantMuestra] = useState('1');
    const [diasRe, setDiasRe] = useState('0');
    const [tipoInforme, setTipoInforme] = useState(null);
    const [organoPrimario, setOrganoPrimario] = useState(null);
    const [organoSecundario, setOrganoSecundario] = useState(null);
    const [patologoRes, setPatologoRes] = useState(null);
    const [tecnologoRes, setTecnologoRes] = useState(null);
    const [calendarValue, setCalendarValue] = useState(null);
    const [tipoBiopsia, setTipoBiopsia] = useState('0');
    const [tipoBiopRapida, setTipoBiopRapida] = useState('0');
    const [citologia, setCitologia] = useState('0');
    const [necropsia, setNecropsia] = useState('0');
    const [placasBr, setPlacasBr] = useState('0');
    const [fTomaMuestra, setFTomaMuestra] = useState(null);
    const [fRecepcionMuestra, setFRecepcionMuestra] = useState(null);
    const [desplegarTabla, setDesplegarTabla] = useState(true);
    const [switchValue, setSwitchValue] = useState(false);
    const [toggleValue, setToggleValue] = useState(false);
    const [toggleEstInforme, setToggleInforme] = useState(false);
    const [hiddenDiv, setHiddenDiv] = useState(false);
    //#region  Constantes

    const Patologos = [
        {
            "id": 1,
            "name": "Dr. Francisco Carpintero"
        },
        {
            "id": 2,
            "name": "Dr. Fernando Lopez"
        },
        {
            "id": 3,
            "name": "Dr. Juan Carlos Paz"
        },
        {
            "id": 4,
            "name": "Dra. Maria de los Angeles"
        },
        {
            "id": 5,
            "name": "Dra. Josefa Ramirez"
        }

    ];
    const Tecnologos = [
        {
            "id": 1,
            "name": "Dr. Roger Salvo"
        },
        {
            "id": 2,
            "name": "Dr. Alvarez Reyes"
        }
    ];
    const Organos = [
        {
            "id": 1,
            "name": "PANCREAS"
        },
        {
            "id": 2,
            "name": "LARINGE"
        },
        {
            "id": 3,
            "name": "MAMA"
        },
        {
            "id": 4,
            "name": "COLON"
        },
        {
            "id": 5,
            "name": "MUSCULO"
        }
    ];
    const TiposInforme = [
        {
            "id": 1,
            "name": "Biopsia"
        },
        {
            "id": 2,
            "name": "Biopsia Rápida"
        },
        {
            "id": 3,
            "name": "Biopsia y Biopsia Rápida"
        },
        {
            "id": 4,
            "name": "Histológica"
        }
    ];
    const PresentMuestra = [
        {
            "id": 1,
            "name": "Frasco"
        },
        {
            "id": 2,
            "name": "Tubos"
        },
        {
            "id": 3,
            "name": "Bolsa"
        },
        {
            "id": 4,
            "name": "Lámina"
        }
    ];
    const ServiciosdeOrigen = [
        {
            "id": 1,
            "name": "Ins. Chileno Japones"
        },
        {
            "id": 2,
            "name": "Dermatología"
        },
        {
            "id": 3,
            "name": "Endocrinología"
        },
        {
            "id": 4,
            "name": "Hematología"
        }
    ];

    const dropdownItems = [
        {
            "id": 1,
            "name": "Fonasa A"
        },
        {
            "id": 2,
            "name": "Fonasa B"
        },
        {
            "id": 3,
            "name": "Fonasa C"
        },
        {
            "id": 4,
            "name": "Fonasa D"
        },
        {
            "id": 5,
            "name": "ISAPRE"
        }
    ];
    //#endregion
    return (
        <div className="p-grid">
            <div className="col-12">
                <div className="card">
                    <h5>Formulario de Biopsia</h5>
                    <hr />
                    <div className="p-fluid formgrid grid">
                        <div className="field col-12 md:col-12">
                            <h2>Datos del paciente</h2>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="lastname2">Rut Paciente</label>
                            <InputText id="lastname2" type="text" />
                        </div>
                        <div className="field col-12 md:col-4">
                            <label htmlFor="firstname2">Nombre Paciente</label>
                            <InputText id="firstname2" type="" />
                        </div>

                        <div className="field col-12 md:col-4">
                            <label htmlFor="MedTratante">Médico Tratante</label>
                            <InputText id="MedTratante" type="text" />
                        </div>
                        <div className="field col-12 md:col-1">
                            {/* <h6>AUGE</h6> */}
                            <label htmlFor="MedTratante">AUGE</label>
                            <ToggleButton id="Aug" name="switchValue" checked={toggleValue} onChange={(e) => setToggleValue(e.value)} onLabel="Si" offLabel="No" />
                        </div>
                        {/* Aca termina el div de 12 */}
                        <div className="field col-12 md:col-1">
                            <label htmlFor="Edad">Edad</label>
                            <InputText id="Edad" type="text" />
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="NroFicha">N° Ficha</label>
                            <InputText id="NroFicha" type="text" />
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="state">Previsión</label>
                            <Dropdown id="state" value={dropdownItem} onChange={(e) => setDropdownItem(e.value)} placeholder="Seleccione" options={dropdownItems} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="ServicioOr">Servicio Origen</label>
                            <Dropdown id="ServicioOr" value={serviciosdeOrigen} onChange={(e) => setServiciosdeOrigen(e.value)} placeholder="Seleccione" options={ServiciosdeOrigen} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="PresMuestra">Presentación</label>
                            <Dropdown id="PresMuestra" value={presentacionMuestra} onChange={(e) => setPresentacionMuestra(e.value)} placeholder="Seleccione" options={PresentMuestra} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Cantidad</label>
                            <InputNumber id="CantMuestra" value={cantMuestra} onValueChange={(e) => setCantMuestra(e.value)} showButtons mode="decimal" min="1"></InputNumber>
                        </div>
                        {/* Aca termina el div de 12 */}
                        <div className="field col-12 md:col-2">
                            <label htmlFor="NroFicha">N° Biopsia</label>
                            <InputText id="NroFicha" type="text" />
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="probDiag">F.Toma Muestra</label>
                            <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fTomaMuestra} onChange={(e) => setFTomaMuestra(e.value)} locale="es"></Calendar>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="probDiag">F.Recepción Muestra</label>
                            <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fRecepcionMuestra} onChange={(e) => setFRecepcionMuestra(e.value)} locale="es"></Calendar>
                        </div>
                        <div className="field col-12 md:col-4">
                            <label htmlFor="PresMuestra">Tipo de Informe</label>
                            <Dropdown id="PresMuestra" value={tipoInforme} onChange={(e) => setTipoInforme(e.value)} placeholder="Seleccione" options={TiposInforme} optionLabel="name"></Dropdown>
                        </div>
                        {/* Aca termina el div de 12 */}
                        <div className="field col-12 md:col-5">
                            <label htmlFor="MuestraEnviada">Muestra enviada</label>
                            <InputTextarea id="MuestraEnviada" placeholder="" autoResize rows="6" cols="30" />
                        </div>
                        <div className="field col-12 md:col-5">
                            <label htmlFor="probDiag">Antecedentes y/o probable diagnostico</label>
                            <InputTextarea id="probDiag" placeholder="" autoResize rows="6" cols="30" />
                        </div>
                        <div className="field col-12 md:col-2">
                            {/* <h6>AUGE</h6> */}
                            <label htmlFor="MedTratante">Informe Procesado</label>
                            <ToggleButton id="Aug" name="switchValue" checked={toggleEstInforme} onChange={(e) => setToggleInforme(e.value)} onLabel="Si" offLabel="No" />
                            <label htmlFor="CantMuestra">Días</label>
                            <InputNumber id="CantMuestra" value={diasRe} onValueChange={(e) => setDiasRe(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        {/* Aca termina el div de 12 */}
                        <div className="field col-12 md:col-3">
                            <label htmlFor="PresMuestra">Organo Primario</label>
                            <Dropdown id="PresMuestra" value={organoPrimario} onChange={(e) => setOrganoPrimario(e.value)} placeholder="Seleccione" options={Organos} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="PresMuestra">Organo Secundario</label>
                            <Dropdown id="PresMuestra" value={organoSecundario} onChange={(e) => setOrganoSecundario(e.value)} placeholder="Seleccione" options={Organos} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="PresMuestra">Patólogo Responsable</label>
                            <Dropdown id="PresMuestra" value={patologoRes} onChange={(e) => setPatologoRes(e.value)} placeholder="Seleccione" options={Patologos} optionLabel="name"></Dropdown>
                        </div>
                        <div className="field col-12 md:col-3">
                            <label htmlFor="PresMuestra">Tecnólogo Responsable</label>
                            <Dropdown id="PresMuestra" value={tecnologoRes} onChange={(e) => setTecnologoRes(e.value)} placeholder="Seleccione" options={Tecnologos} optionLabel="name"></Dropdown>
                        </div>
                        {/* Aca termina el div de 12 */}

                        <div className="field col-12 md:col-12">
                            <hr />
                            <h5>Tipo de Examen</h5>
                        </div>
                        {/* Aca termina el div de 12 */}
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Biopsia</label>
                            <InputNumber id="CantMuestra" value={tipoBiopsia} onValueChange={(e) => setTipoBiopsia(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        <div className="field col-12 md:col-2">

                            <label htmlFor="CantMuestra">Biopsia Rápida</label>
                            <InputNumber id="CantMuestra" value={tipoBiopRapida} onValueChange={(e) => setTipoBiopRapida(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Citología</label>
                            <InputNumber  id="CantMuestra" value={citologia} onValueChange={(e) => setCitologia(e.value)} showButtons mode="decimal" min="0" ></InputNumber>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Necropsia</label>
                            <InputNumber id="CantMuestra" value={necropsia} onValueChange={(e) => setNecropsia(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Placas BR</label>
                            <InputNumber id="CantMuestra" value={placasBr} onValueChange={(e) => setPlacasBr(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        <div className="field col-12 md:col-2">
                            <label htmlFor="CantMuestra">Placas BR</label>
                            <InputNumber id="CantMuestra" value={placasBr} onValueChange={(e) => setPlacasBr(e.value)} showButtons mode="decimal" min="0"></InputNumber>
                        </div>
                        {/* Aca termina el div de 12 */}
                        {/* <div className="field col-12 md:col-2" float="right">
                            <Image src="assets/layout/images/LogoHCSBA.png" alt="galleria" width={200} />
                        </div> */}
                        <div className="field col-12 md:col-2">
                            <Button label="Codigos Fonasa" className="mr-2 mb-2" />
                        </div>
                        <div className="field col-12 md:col-2">
                            <Button label="Generar Informe" className="mr-2 mb-2" />
                        </div>
                        <div className="field col-12 md:col-2">
                            <Button label="Macro/Micro" className="mr-2 mb-2" />
                        </div>
                        <div className="field col-12 md:col-2">
                            <Button label="Diagnostico" className="mr-2 mb-2" />
                        </div>



                    </div>
                </div>
            </div>

        </div>

    );
};
// const comparisonFn = function (prevProps, nextProps) {
//     return prevProps.location.pathname === nextProps.location.pathname;
// };
export default React.memo(FormCrearBiopsias);

import { Column } from "jspdf-autotable";
import { Button } from "primereact/button";
import { DataTable } from "primereact/datatable";
import { Dialog } from "primereact/dialog";
import { Toast } from "primereact/toast";
import React, { useState, useEffect, useRef } from "react";
import { searchDiagnosticosById, searchMacroMicro } from "../InformeBiopsia/InformeBiopsiaService";
import { listOrganos } from "./RegistroBiobsiaService";
import VistaInformeBiopsia from "./VistaInformeBiopsia";

const TableBiopsiasPaciente = ({ biopsiasPaciente }) => {
    const [displayBasic2, setDisplayBasic2] = useState(false);
    const [filterData, setFilterData] = useState({});
    useEffect(() => {
        console.log(biopsiasPaciente);
    }, []);


    const editProduct = async (product) => {
        console.log(product);
        let organos = await listOrganos();
        console.log(organos);
        organos = organos.filter(organos => organos.id === product.organo1);
        console.log(organos);
        let macromicro = null;
        try {
            macromicro = await searchMacroMicro(product.idBiopsia);
        } catch (error) {

        }

        console.log(macromicro);
        let diag = null;
        try {
            diag = await searchDiagnosticosById(product.idBiopsia);
        } catch (error) {

        }
        let microscopia
        let macroscopia
        if (macromicro) {
            if (macromicro.macroscopia !== "" && macromicro.macroscopia !== undefined && macromicro.macroscopia !== null) {
                macroscopia = macromicro.macroscopia.replace(/<[^>]+>/g, '');
            }
            if (macromicro.microscopia !== "" && macromicro.microscopia !== undefined && macromicro.microscopia !== null) {
                microscopia = macromicro.microscopia.replace(/<[^>]+>/g, '');
            }
        }

        let viewData = {
            id: product.idBiopsia,
            nrobiopsia: product.nrobiopsia,
            organo: organos[0].nombreOrgano,
            macro : macroscopia,
            micro : microscopia,
            diag : diag.diagnostico,
            nota : diag.notas

        };
        console.log(diag);
        console.log(viewData);
        setFilterData({...viewData});

        onClick('displayBasic2');
    }
    const [position, setPosition] = useState('center');
    const onClick = (name, position) => {
        dialogFuncMap[`${name}`](true);

        if (position) {
            setPosition(position);
        }
    }
    const dialogFuncMap = {
        'displayBasic2': setDisplayBasic2,
    }
    const onHide = (name) => {
        dialogFuncMap[`${name}`](false);
    }
    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-search-plus" className="p-button-rounded p-button-success mr-2" onClick={() => editProduct(rowData)} />
            </React.Fragment>
        );
    }
    return (
        <React.Fragment>
            <div className="p-grid">
                <div className="col-12">
                    <div className="card">
                        <h5>Visualizador de Biopsias </h5>
                        {/* <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" icon={<FaStethoscope />} className="p-button-raised" onClick={() => onClick('dialogDiagnostico')} />
                                </div>
                            </div> */}
                        <hr />
                        <h2>Biopsias Del Paciente</h2>
                        <div className="p-fluid formgrid grid">
                            <DataTable value={biopsiasPaciente} sortField="fechainforme" sortOrder={-1} responsiveLayout="scroll">
                                <Column field="idBiopsia" header="id" sortable></Column>
                                <Column field="nrobiopsia" header="N° Biopsia" sortable></Column>
                                <Column field="fecharecepcionmuestra" header="Recepción Muestra" sortable></Column>
                                <Column field="fechatomamuestra" header="Toma Muestra" sortable></Column>
                                <Column field="fechainforme" header="Fecha Informe" sortable></Column>
                                <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>
            <Dialog header="" visible={displayBasic2} maximizable modal style={{ width: '50vw', height: '30vw' }} onHide={() => onHide('displayBasic2')}>
                    <VistaInformeBiopsia biopsiasPaciente = {filterData}/>
            </Dialog>
        </React.Fragment>
    )
}
export default TableBiopsiasPaciente;

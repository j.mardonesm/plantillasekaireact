import React, { useState, useEffect, useRef } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { InputTextarea } from "primereact/inputtextarea";
import { Dropdown } from "primereact/dropdown";
import { Dialog } from 'primereact/dialog';
import TablaCodigoFonasa from "../CodigosFonasa/TablaCodigoFonasa";
import MacroMicro from "../MacroMicro/MacroMicro";
import { ToggleButton } from "primereact/togglebutton";
import { classNames } from "primereact/utils";
import { Calendar } from "primereact/calendar";
import { useForm, Controller } from "react-hook-form";
import { FaRegFileAlt } from "@react-icons/all-files/fa/FaRegFileAlt";
import { FaSave } from "@react-icons/all-files/fa/FaSave";
import { FaStethoscope } from "@react-icons/all-files/fa/FaStethoscope";
import { GiMicroscope } from "@react-icons/all-files/gi/GiMicroscope";
import { FaMediumM } from "@react-icons/all-files/fa/FaMediumM";
import { formatoMuestra, listFormatoInforme, listOrganos, listServicios, searchNroFichaById, searchPrevision, searchPrevisionByid, servicioById, updateBiopsia } from "./RegistroBiobsiaService";
import { searchByNroBiopsia } from "./RegistroBiobsiaService";
import { searchPersonaById } from "./RegistroBiobsiaService";
import { locale, addLocale } from "primereact/api";
import { AutoComplete } from "primereact/autocomplete";
import { Toast } from "primereact/toast";
import Swal from "sweetalert2";
import Diagnosticos from "../Diagnostico/Diagnosticos";
import MaterialExaminado from "../MaterialExaminado/MaterialExaminado";
import { OverlayPanel } from 'primereact/overlaypanel';
import InformeBiopsia from "../InformeBiopsia/InformeBiopsia";
import TableBiopsiasPaciente from "./TableBiopsiasPaciente";

addLocale("es", {
    firstDayOfWeek: 1,
    dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
    today: "Hoy",
    clear: "Limpiar",
});

locale("es");

const FormBuscarBiopsia = ({ nroBiopsia, InfoPersona, idBiopsia }) => {
    const [edadPersona, setEdadPersona] = useState("");
    const [nroFicha, setNroFicha] = useState("");
    const [person, setPerson] = useState([]);
    const [serviciosOrigen, setServiciosOrigen] = useState([]);
    const [formatoDeMuestra, setFormatoDeMuestra] = useState([]);
    const [listaDeOrganos, setListaDeOrganos] = useState([]);
    const [listFormatoInform, setListFormatoInform] = useState([]);
    const [biopsiaBuscada, setBiopsiaBuscada] = useState({});
    const [dialogCodFonasa, setDialogCodFonasa] = useState(false);
    const [dialogMacroMicro, setDialogMacroMicro] = useState(false);
    const [dialogInformeBiopsia, setDialogInformeBiopsia] = useState(false);
    const [dialogDiagnostico, setDialogDiagnostico] = useState(false);
    const [dialogMateriales, setDialogMateriales] = useState(false);
    const [filteredMeds, setFilteredMeds] = useState(null);
    const [filteredServices, setFilteredServices] = useState(null);
    const [previsiones, setPrevisiones] = useState([]);
    const [biopsiaid, setBiopsiaId] = useState(null);
    const [pacienteId, setPacienteId] = useState(null);
    const [resultBio, setResultBio] = useState([]);
    const [EstadoInforme, setEstadoInforme] = useState(false);
    const [disableFecha1, setDisableFecha1] = useState(false);

    const op = useRef(null);
    const op2 = useRef(null);

    const [loading3, setLoading3] = useState(false);
    const toast = useRef(null);
    const showSuccess = () => {
        toast.current.show({ severity: 'success', summary: 'Guardado', detail: 'La información fue guardada con exito', life: 3000 });
    }
    const searchServicios = (event) => {
        setTimeout(() => {
            let _filteredServicios;
            if (!event.query.trim().length) {
                _filteredServicios = [...serviciosOrigen];
            }
            else {
                _filteredServicios = serviciosOrigen.filter((servicio) => {
                    return servicio.descripcion.toLowerCase().startsWith(event.query.toLowerCase());
                });
            }

            setFilteredServices(_filteredServicios);
        }, 250);
    }
    const searchMeds = (event) => {
        setTimeout(() => {
            let _filteredServicios;
            if (!event.query.trim().length) {
                _filteredServicios = [...Patologos];
            }
            else {
                _filteredServicios = Patologos.filter((servicio) => {
                    return servicio.name.toLowerCase().startsWith(event.query.toLowerCase());
                });
            }

            setFilteredMeds(_filteredServicios);
        }, 250);
    }
    const biopsias = {
        id: "",
        nrobiopsia: "",
        auge: "",
        personaId: "",
        medicotratante: "",
        fechatomamuestra: "",
        fecharecepcionmuestra: "",
        servicio: "",
        organo1: "",
        organo2: "",
        patologo: "",
        tecnologo: "",
        dias: "",
        fechainforme: "",
        fechaentregainforme: "",
        biopsia: "",
        biopsiarapida: "",
        citologico: "",
        necropsia: "",
        placas: "",
        muestraenviada: "",
        antecedentes: "",
        tipoinformeid: "",
        formatomuestraid: "",
    };
    const [position, setPosition] = useState('center');

    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();


    const DialogFonasa = {
        'dialogCodFonasa': setDialogCodFonasa,
        'dialogMacroMicro': setDialogMacroMicro,
        'dialogDiagnostico': setDialogDiagnostico,
        'dialogMateriales': setDialogMateriales,
        'dialogInformeBiopsia': setDialogInformeBiopsia,
    }
    const onClick = (name, position) => {
        DialogFonasa[`${name}`](true);

        if (position) {
            setPosition(position);
        }
    }

    const onHide = (name) => {
        DialogFonasa[`${name}`](false);
    }

    const renderFooter = (name) => {
        return (
            <div>
                {/* <Button label="No" icon="pi pi-times" onClick={() => onHide(name)} className="p-button-text" /> */}
                <Button label="Cerrar" icon="pi pi-times" onClick={() => onHide(name)} autoFocus />
            </div>
        );
    }


    const renderHeaderCodFonasa = () => {
        return (
            <span className="ql-formats">
                <button className="ql-bold" aria-label="Bold"></button>
                <button className="ql-italic" aria-label="Italic"></button>
                <button className="ql-underline" aria-label="Underline"></button>
            </span>
        );
    }
    const headerCodFonasa = renderHeaderCodFonasa();
    // const renderFooterCodFonasa = (name) => {
    //     return (
    //         <div>
    //             <Button label="No" icon="pi pi-times" onClick={() => onHide(name)} className="p-button-text" />
    //             <Button label="Yes" icon="pi pi-check" onClick={() => onHide(name)} autoFocus />
    //         </div>
    //     );
    // }
    const submitBiopsia = async (data) => {
        console.log(data);
        let FechaEntInforme;
        let FechaInforme;
        let FechaRecepMuestra;
        let FechaTomaMuestra;
        FechaEntInforme = data.FechaEntInforme;
        FechaInforme = data.FechaInforme;
        FechaRecepMuestra = data.FechaRecepMuestra;
        FechaTomaMuestra = data.FechaTomaMuestra;
        // FechaEntInforme = new Date(FechaEntInforme.getFullYear() + "-" + (FechaEntInforme.getMonth() + 1) + "-" + (FechaEntInforme.getDate() + 1));
        // FechaInforme = new Date(FechaInforme.getFullYear() + "-" + (FechaInforme.getMonth() + 1) + "-" + (FechaInforme.getDate() + 1));
        // FechaRecepMuestra = new Date(FechaRecepMuestra.getFullYear() + "-" + (FechaRecepMuestra.getMonth() + 1) + "-" + (FechaRecepMuestra.getDate() + 1));
        // FechaTomaMuestra = new Date(FechaTomaMuestra.getFullYear() + "-" + (FechaTomaMuestra.getMonth() + 1) + "-" + (FechaTomaMuestra.getDate() + 1));
        let FechaEntInformeFormated;
        let FechaInformeFormated;
        let FechaRecepMuestraFormated;
        let FechaTomaMuestraFormated;
        console.log(FechaEntInforme);
        //#region Formateo Fecha
        if (data.FechaEntInforme === undefined || data.FechaEntInforme === "" || data.FechaEntInforme === null ) {
            FechaEntInforme = "";
        } else {
            FechaEntInforme = new Date(FechaEntInforme.getFullYear() + "-" + (FechaEntInforme.getMonth()+ 1) + "-" + (FechaEntInforme.getDate()));
            FechaEntInformeFormated = FechaEntInforme;
            console.log(FechaEntInformeFormated);
            FechaEntInforme = new Intl.DateTimeFormat('fr-CA', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(FechaEntInformeFormated);
        }
        if (data.FechaInforme === undefined || data.FechaInforme === "" || data.FechaInforme === null) {
            FechaInforme = "";
        } else {
            FechaInforme = new Date(FechaInforme.getFullYear() + "-" + (FechaInforme.getMonth() + 1) + "-" + (FechaInforme.getDate()));
            FechaInformeFormated = FechaInforme;
            FechaInforme = new Intl.DateTimeFormat('fr-CA').format(FechaInformeFormated);
        }
        if (data.FechaRecepMuestra === undefined || data.FechaRecepMuestra === "" || data.FechaRecepMuestra === null) {
            FechaRecepMuestra = "";
        } else {
            FechaRecepMuestra = new Date(FechaRecepMuestra.getFullYear() + "-" + (FechaRecepMuestra.getMonth() + 1) + "-" + (FechaRecepMuestra.getDate()));
            FechaRecepMuestraFormated = FechaRecepMuestra;
            FechaRecepMuestra = new Intl.DateTimeFormat('fr-CA').format(FechaRecepMuestraFormated);
        }
        if (data.FechaTomaMuestra === undefined || data.FechaTomaMuestra === "" || data.FechaTomaMuestra === null) {
            FechaTomaMuestra = "";
        } else {
            FechaTomaMuestra = new Date(FechaTomaMuestra.getFullYear() + "-" + (FechaTomaMuestra.getMonth() + 1) + "-" + (FechaTomaMuestra.getDate()));
            FechaTomaMuestraFormated = FechaTomaMuestra;
            FechaTomaMuestra = new Intl.DateTimeFormat('fr-CA').format(FechaTomaMuestraFormated);
        }
        setEstadoInforme(data.InformeProcesado);
        if((data.FechaEntInforme !== undefined && data.FechaEntInforme !== "" && data.FechaEntInforme !== null) && (data.FechaInforme !== undefined && data.FechaInforme !== "" && data.FechaInforme !== null) && (data.FechaRecepMuestra !== undefined && data.FechaRecepMuestra !== "" && data.FechaRecepMuestra !== null) && (data.FechaTomaMuestra !== undefined && data.FechaTomaMuestra !== "" && data.FechaTomaMuestra !== null)){
            setDisableFecha1(true);
        }
        //#endregion
        console.log(FechaEntInforme);
        //#region Asignacion de variables
        biopsias.antecedentes = data.AntecedentesDiag;
        if (data.prevision === undefined || data.prevision === null) {
            biopsias.prevision = "";
        } else {
            biopsias.prevision = data.prevision.id;
        }
        if (data.PresMuestra === undefined || data.PresMuestra === null) {
            biopsias.formatomuestraid = "";
        } else {
            biopsias.formatomuestraid = data.PresMuestra.id;
        }
        biopsias.necropsia = data.NecropsiaTipo;
        if (data.OrganoPrimario === undefined || data.OrganoPrimario === null) {
            biopsias.organo1 = "";
        } else {
            biopsias.organo1 = data.OrganoPrimario.id;
        }
        if (data.OrganoSecundario === undefined || data.OrganoSecundario === null) {
            biopsias.organo2 = "";
        } else {
            biopsias.organo2 = data.OrganoSecundario.id;
        }
        if (data.PatologoResp === undefined || data.PatologoResp === null) {
            biopsias.patologo = "";
        } else {
            biopsias.patologo = data.PatologoResp.id;
        }
        if (data.ServicioOr === undefined || data.ServicioOr === null) {
            biopsias.servicio = "";
        } else {
            biopsias.servicio = data.ServicioOr.id;
        }
        if (data.TecnologoResp === undefined || data.TecnologoResp === null) {
            biopsias.placas = "";
        } else {
            biopsias.tecnologo = data.TecnologoResp.id;
        }
        if (data.TipoInforme === undefined || data.TipoInforme === null) {
            biopsias.tipoinformeid = "";
        } else {
            biopsias.tipoinformeid = data.TipoInforme.id;
        }
        if (data.MedTratante === undefined || data.MedTratante === null) {
            biopsias.medicotratante = "";
        } else {
            biopsias.medicotratante = data.MedTratante.id;
        }
        biopsias.biopsia = data.BiopsiaTipo;
        biopsias.informeprocesado = data.InformeProcesado;
        biopsias.biopsiarapida = data.BiopsiaRapidaTipo;
        biopsias.citologico = data.CitologiaTipo;
        biopsias.dias = data.DiasRecep;
        biopsias.fechaentregainforme = FechaEntInforme;
        biopsias.fechainforme = FechaInforme;
        biopsias.fecharecepcionmuestra = FechaRecepMuestra;
        biopsias.id = biopsiaid;
        biopsias.placas = data.PlacasBrTipo;
        biopsias.muestraenviada = data.MuestraEnviada;
        biopsias.cantidadmuestra = data.CantMuestra;
        biopsias.nrobiopsia = data.nroBiopsia;
        biopsias.fechatomamuestra = FechaTomaMuestra;
        biopsias.auge = data.Auge;
        biopsias.personaId = pacienteId;
        console.log(biopsias);
        //#endregion
        let upFicha = await updateBiopsia(biopsias, biopsiaid);
        let biop = {}
        biop ={
            antecedentes: biopsias.antecedentes,
            auge: biopsias.auge,
            biopsia: biopsias.biopsia,
            biopsiarapida: biopsias.biopsiarapida,
            citologico: biopsias.citologico,
            dias: biopsias.dias,
            fechaentregainforme: data.FechaEntInforme,
            fechainforme: data.FechaInforme,
            fecharecepcionmuestra: data.FechaRecepMuestra,
            fechatomamuestra: data.FechaTomaMuestra,
            id: biopsias.id,
            medicotratante: biopsias.medicotratante,
            muestraenviada: biopsias.muestraenviada,
            necropsia: biopsias.necropsia,
            nroBiopsia: biopsias.nrobiopsia,
            patologo: biopsias.patologo,
            personaId: biopsias.personaId,
            placas: biopsias.placas,
            servicio: biopsias.servicio,
            tecnologo: biopsias.tecnologo,
            prevision: biopsias.prevision,

        };
        let _biop = {...biop};
        console.log(_biop);
        setBiopsiaBuscada(_biop);
        onLoadingClick3();
        console.log(upFicha);
        setResultBio(upFicha);
        console.log(biopsiaBuscada);
        if (upFicha) {
            showSuccess();

        } else {
            successAlert();
        }

    };
    const successAlert = () => {
        Swal.fire({
            title: 'Alerta!',
            text: 'Error al guardar',
            icon: 'warning',
        });
    };
    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };
    useEffect(() => {
        search();

    }, []);

    const onLoadingClick3 = async () => {
        setLoading3(true);
        //desplegarTabla(true);

        setTimeout(() => {
            setLoading3(false);
        }, 4000);
    };
    //#region Busca persona por rut
    const search = async () => {
        await Object.assign(person, InfoPersona);
        console.log("BuscandoBiopsia");
        console.log(nroBiopsia);
        let result = await searchByNroBiopsia(nroBiopsia);
        if (result) {
            console.log(result);
            //#region Formateo Fechas
            let FechaEntInforme;
            let FechaInforme;
            let FechaRecepMuestra;
            let FechaTomaMuestra;
            console.log(result[0].fechaentregainforme);
            console.log(result[0].fechainforme);
            console.log(result[0].fecharecepcionmuestra);
            console.log(result[0].fechatomamuestra);
            if (result[0].fechaentregainforme === undefined || result[0].fechaentregainforme === null) {
                FechaEntInforme = "";
            }
            else {
                FechaEntInforme = result[0].fechaentregainforme + "T00:00:00";
                FechaEntInforme = new Date(FechaEntInforme);
            }
            if (result[0].fechainforme === undefined || result[0].fechainforme === null) {
                FechaInforme = "";
            } else {
                FechaInforme = result[0].fechainforme + "T00:00:00";
                FechaInforme = new Date(FechaInforme);
            }
            if (result[0].fecharecepcionmuestra === undefined || result[0].fecharecepcionmuestra === null) {
                FechaRecepMuestra = "";
            } else {
                FechaRecepMuestra = result[0].fecharecepcionmuestra + "T00:00:00";
                FechaRecepMuestra = new Date(FechaRecepMuestra);
            }
            if (result[0].fechatomamuestra === undefined || result[0].fechatomamuestra === null) {
                FechaTomaMuestra = "";
            } else {
                FechaTomaMuestra = result[0].fechatomamuestra + "T00:00:00";
                FechaTomaMuestra = new Date(FechaTomaMuestra);
            }
            if((result[0].fechaentregainforme !== undefined && result[0].fechaentregainforme !== "" && result[0].fechaentregainforme !== null) && (result[0].fechainforme !== undefined && result[0].fechainforme !== "" && result[0].fechainforme !== null) && (result[0].fecharecepcionmuestra !== undefined && result[0].fecharecepcionmuestra !== "" && result[0].fecharecepcionmuestra !== null) && (result[0].fechatomamuestra !== undefined && result[0].fechatomamuestra !== "" && result[0].fechatomamuestra !== null)){
                setDisableFecha1(true);
            }
            console.log(FechaEntInforme);
            console.log(FechaInforme);
            console.log(FechaRecepMuestra);
            console.log(FechaTomaMuestra);
            //#endregion
            console.log(result);
            setResultBio(result);
            setBiopsiaId(result[0].id);
            let idPaciente = result[0].personaId;
            setPacienteId(result[0].personaId);
            let paciente = await searchPersonaById(idPaciente);
            await Object.assign(person, paciente);
            console.log(paciente);
            setPerson(paciente);
            let edad = paciente.fechaNacimiento;
            let id = paciente.id;
            let biop = {};
            let idServicio = "";
            if (result[0].servicio !== undefined && result[0].servicio != null) {
                idServicio = await servicioById(result[0].servicio);
                console.log(idServicio);
            }
            let idPrevision = "";
            if (result[0].prevision !== undefined && result[0].prevision != null) {
                idPrevision = await searchPrevisionByid(result[0].prevision);
                console.log(idPrevision);
            }
            edadCalc(edad);
            let formatoMuestraID;
            let organo1Get;
            let organo2Get;
            let muestra = await formatoMuestra();
            let presentacion = [...muestra];
            setFormatoDeMuestra(presentacion);
            for (var x in muestra) {
                if (muestra[x].id === result[0].formatomuestraid) {
                    formatoMuestraID = muestra[x];
                }
            }
            let organos = await listOrganos();
            setListaDeOrganos(organos);
            for (var y in organos) {
                if (organos[y].id === result[0].organo1) {
                    organo1Get = organos[y];
                }
                if (organos[y].id === result[0].organo2) {
                    organo2Get = organos[y];
                }
            }
            let formInforme = await listFormatoInforme();
            setListFormatoInform(formInforme);
            let informesList;
            for (var t in formInforme) {
                if (formInforme[t].id === result[0].tipoinformeid) {
                    informesList = formInforme[t];
                }
            }

            console.log(formatoMuestraID);
            reset({
                nroBiopsia: result[0].nrobiopsia,
                MedTratante: result[0].medicotratante,
                Auge: result[0].auge,
                prevision: idPrevision,
                PresMuestra: formatoMuestraID,
                CantMuestra: result[0].cantidadmuestra,
                FechaEntInforme: FechaEntInforme,
                FechaInforme: FechaInforme,
                FechaTomaMuestra: FechaTomaMuestra,
                FechaRecepMuestra: FechaRecepMuestra,
                MuestraEnviada: result[0].muestraenviada,
                AntecedentesDiag: result[0].antecedentes,
                OrganoPrimario: organo1Get,
                OrganoSecundario: organo2Get,
                PatologoResp: result[0].patologo,
                TecnologoResp: result[0].tecnologo,
                BiopsiaTipo: result[0].biopsia,
                BiopsiaRapidaTipo: result[0].biopsiarapida,
                CitologiaTipo: result[0].citologico,
                NecropsiaTipo: result[0].necropsia,
                PlacasBrTipo: result[0].placas,
                ServicioOr: idServicio,
                TipoInforme: informesList,
                InformeProcesado: result[0].informeprocesado,
                DiasRecep: result[0].dias
            });
            let ficha = await searchNroFichaById(id);
            biop = {
                antecedentes: result[0].antecedentes,
                auge: result[0].auge,
                biopsia: result[0].biopsia,
                biopsiarapida: result[0].biopsiarapida,
                citologico: result[0].citologico,
                dias: result[0].dias,
                fechaentregainforme: FechaEntInforme,
                fechainforme: FechaInforme,
                fecharecepcionmuestra: FechaRecepMuestra,
                fechatomamuestra: FechaTomaMuestra,
                formatomuestraid: formatoMuestraID,
                id: result[0].id,
                medicotratante: result[0].medicotratante,
                muestraenviada: result[0].muestraenviada,
                necropsia: result[0].necropsia,
                nroBiopsia: result[0].nrobiopsia,
                organo1: organo1Get,
                organo2: organo2Get,
                patologo: result[0].patologo,
                personaId: result[0].personaId,
                placas: result[0].placas,
                servicio: result[0].servicio,
                tecnologo: result[0].tecnologo,
                tipoinformeid: informesList,
                prevision: result[0].prevision,

            };
            let _biop = {...biop};
            console.log(_biop);
            setBiopsiaBuscada(_biop);
            setEstadoInforme(result[0].informeprocesado);
            console.log(biop);
            console.log(ficha);
            let idFicha;
            let nroDeFicha;
            if (ficha) {
                ficha.forEach((e) => {
                    nroDeFicha = e.ficha;
                    idFicha = e.id;
                    setNroFicha(nroDeFicha);
                });

            } else {
                setNroFicha("no informado");
            }
            let serviciosHospital = await listServicios();
            setServiciosOrigen(serviciosHospital);
            let prevision = await searchPrevision();
            setPrevisiones(prevision);
            console.log(nroFicha);
        } else {
            successAlert();
        }

    };
    //#endregion
    //#region Calcula la edad
    const edadCalc = async (fecha) => {
        let hoy = new Date();
        let fechaNacimiento = new Date(fecha);
        let mesf = fechaNacimiento.getMonth() + 1;
        let aniof = fechaNacimiento.getFullYear();
        let diaf = fechaNacimiento.getDate();

        if (isNaN(mesf)) {
            alert("No se pudo calcular la edad");
            return;
        }
        let mesh = hoy.getMonth() + 1;
        let anioh = hoy.getFullYear();
        let diah = hoy.getDate();

        let diasSum = diasFin(aniof, mesf);

        let mesAdicional = 0;

        if (diaf > diah) {
            diah = parseInt(diah) + parseInt(diasSum);
            mesAdicional = 1;
        }

        let dias = diah - diaf;

        let anioAdicional = 0;

        if (mesf > mesh) {
            mesh = parseInt(mesh) + 12;
            anioAdicional = 1;
        }

        let meses = parseInt(mesh) - (parseInt(mesf) + parseInt(mesAdicional));
        let anios = parseInt(anioh) - (parseInt(aniof) + parseInt(anioAdicional));

        setEdadPersona(anios + " años " + meses + " meses " + dias + " días");
    };
    let mesSig = "";
    const diasFin = (anio, mes) => {
        let anionAux = anio;
        if (anio < 12) {
            mesSig = parseInt(mes) + 1;
        } else {
            mesSig = 1;
            anionAux = parseInt(anio) + 1;
        }

        let fecha = anionAux + "-" + mesSig + "-" + "01";

        let ms = Date.parse(fecha);

        let fechaD = new Date(ms);
        fechaD.setDate(fechaD.getDate() - 1);

        let diaH = fechaD.getDate();
        return diaH;
    };
    //#endregion
    let today = new Date();
    //#region  Constantes

    const Patologos = [
        {
            id: 1,
            name: "Dr. Francisco Carpintero",
        },
        {
            id: 2,
            name: "Dr. Fernando Lopez",
        },
        {
            id: 3,
            name: "Dr. Juan Carlos Paz",
        },
        {
            id: 4,
            name: "Dra. Maria de los Angeles",
        },
        {
            id: 5,
            name: "Dra. Josefa Ramirez",
        },
        {
            id: 6,
            name: "Dr. Juan Carlos Manuel Mardones Montiel",
        },
    ];
    const Tecnologos = [
        {
            id: 1,
            name: "Dr. Roger Salvo",
        },
        {
            id: 2,
            name: "Dr. Alvarez Reyes",
        },
    ];

    //#endregion
    return (
        <React.Fragment>
            <form onSubmit={handleSubmit(submitBiopsia)}>
                <div className="p-grid">
                    <div className="col-12">
                        <div className="card">
                            <Toast ref={toast} position="bottom-center" />
                            <h5>Formulario de Biopsia </h5>
                            {/* <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" icon={<FaStethoscope />} className="p-button-raised" onClick={(e) => op.current.toggle(e)} />
                                </div>
                            </div> */}
                            <hr />
                            <h2>Datos Del Paciente</h2>
                            <div className="p-fluid formgrid grid">

                                <React.Fragment>
                                    <div className="field col-12 md:col-2">
                                        <strong>Rut Paciente</strong>
                                        <h6>{person.nroDocumento}</h6>
                                    </div>
                                    <div className="field col-12 md:col-3">
                                        <strong>Nombre Paciente</strong>
                                        <h6>
                                            {person.primerNombre} {person.apellidoPaterno} {person.apellidoMaterno}
                                        </h6>
                                    </div>
                                    <div className="field col-12 md:col-3">
                                        <strong>Edad</strong>
                                        <h6 >{edadPersona}</h6>
                                    </div>
                                </React.Fragment>


                                <div className="field col-12 md:col-1">
                                    <strong>N° Ficha</strong>
                                    <h6>{nroFicha}</h6>
                                </div>

                                {/* <div className="field col-12 md:col-2">
                            <strong>N° Biopsia</strong>
                            <h6>22-0025</h6>
                        </div> */}

                                <div className="field col-12 md:col-12">
                                    <hr />
                                </div>
                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-12">
                                    <h2>Recepción de Muestra</h2>
                                </div>

                                <div className="field col-12 md:col-12">
                                    <div className="field col-12 md:col-2">
                                        <label htmlFor="nroBiopsia">Nro Biopsia</label>
                                        <Controller name="nroBiopsia" control={control} rules={{ required: "El Nro de Biopsia es Obligatorio" }} render={({ field, fieldState }) =>
                                            <InputText id={field.name} {...field} readOnly className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                        {/* <InputText id="NroBiopsia" type="text" /> */}
                                    </div>
                                    {getFormErrorMessage("nroBiopsia")}
                                </div>

                                <div className="field col-12 md:col-4">
                                    <label htmlFor="MedTratante">Médico Tratante</label>
                                    <Controller name="MedTratante" control={control} render={({ field }) => (
                                        <AutoComplete id={field.name} {...field} value={field.value} suggestions={filteredMeds} completeMethod={searchMeds} onChange={(e) => field.onChange(e.target.value)} field="name" dropdown forceSelection />
                                    )} />
                                    {/* <label htmlFor="MedTratante">Médico Tratante</label>
                                    <Controller name="MedTratante" control={control} render={({ field, fieldState }) =>
                                        <InputText id={field.name} {...field} className={classNames({ "p-invalid": fieldState.invalid })} />} /> */}
                                    {/* <InputText id="MedTratante" type="text" /> */}
                                </div>
                                <div className="field col-12 md:col-1">
                                    {/* <h6>AUGE</h6> */}
                                    <label htmlFor="Auge">AUGE</label>
                                    <Controller name="Auge" defaultValue={false} control={control} render={({ field, fieldState }) =>
                                        <ToggleButton id={field.name} {...field} onChange={(e) => field.onChange(e.value)} checked={field.value} onLabel="Si" offLabel="No" />} />
                                    {/* <ToggleButton id="Aug" name="switchValue" checked={toggleValue} onChange={(e) => setToggleValue(e.value)} onLabel="Si" offLabel="No" /> */}
                                </div>
                                {/* Aca termina el div de 12 */}

                                <div className="field col-12 md:col-2">
                                    <label htmlFor="prevision">Previsión</label>
                                    <Controller name="prevision" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={previsiones} optionLabel="descripcion" />
                                    )} />

                                    {/* <Dropdown id="state" value={dropdownItem} onChange={(e) => setDropdownItem(e.value)} placeholder="Seleccione" options={dropdownItems} optionLabel="name"></Dropdown> */}
                                </div>
                                <div className="field col-12 md:col-4">
                                    <label htmlFor="ServicioOr">Servicio Origen</label>
                                    <Controller name="ServicioOr" control={control} render={({ field }) => (
                                        <AutoComplete id={field.name} {...field} value={field.value} suggestions={filteredServices} completeMethod={searchServicios} onChange={(e) => field.onChange(e.target.value)} field="descripcion" dropdown forceSelection />
                                    )} />
                                    {/* <label htmlFor="ServicioOr">Servicio Origen</label>
                                    <Controller name="ServicioOr" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={ServiciosdeOrigen} optionLabel="name" />
                                    )} /> */}
                                    {/* <Dropdown id="ServicioOr" value={serviciosdeOrigen} onChange={(e) => setServiciosdeOrigen(e.value)} placeholder="Seleccione" options={ServiciosdeOrigen} optionLabel="name"></Dropdown> */}
                                </div>
                                <div className="field col-12 md:col-2">
                                    <label htmlFor="PresMuestra">Presentación</label>
                                    <Controller name="PresMuestra" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={formatoDeMuestra} optionLabel="formato" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={presentacionMuestra} onChange={(e) => setPresentacionMuestra(e.value)} placeholder="Seleccione" options={PresentMuestra} optionLabel="name"></Dropdown> */}
                                </div>
                                <div className="field col-12 md:col-1">
                                    <label htmlFor="CantMuestra">Cantidad</label>
                                    <Controller name="CantMuestra" defaultValue={1} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="1" />
                                    )} />
                                    {/* <InputNumber id="CantMuestra" value={cantMuestra} onValueChange={(e) => setCantMuestra(e.value)} showButtons mode="decimal" min="1"></InputNumber> */}
                                </div>

                                <div className="field col-12 md:col-4">
                                    <label htmlFor="FechaTomaMuestra">F.Toma Muestra</label>
                                    <Controller name="FechaTomaMuestra" control={control} render={({ field }) => (
                                        <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" />
                                    )} />
                                    {/* <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fTomaMuestra} onChange={(e) => setFTomaMuestra(e.value)} locale="es"></Calendar> */}
                                </div>
                                <div className="field col-12 md:col-4">
                                    <label htmlFor="FechaRecepMuestra">F.Recepción Muestra</label>
                                    <Controller name="FechaRecepMuestra" control={control} render={({ field }) => (
                                        <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" disabledDays={[0, 6]} maxDate={today} readOnlyInput />
                                    )} />
                                    {/* <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fRecepcionMuestra} onChange={(e) => setFRecepcionMuestra(e.value)} locale="es"></Calendar> */}
                                </div>
                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-12">
                                    <hr />
                                </div>
                                {/* Salto de linea */}
                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-12">
                                    <h2>Detalle de biopsia</h2>
                                </div>
                                <div className="field col-12 md:col-6">
                                    <label htmlFor="MuestraEnviada">Muestra enviada</label>
                                    <Controller name="MuestraEnviada" control={control} render={({ field }) => (
                                        <InputTextarea id={field.name} {...field} placeholder="" autoResize rows="6" cols="30" />
                                    )} />
                                    {/* <InputTextarea id="MuestraEnviada" placeholder="" autoResize rows="6" cols="30" /> */}
                                </div>
                                <div className="field col-12 md:col-6">
                                    <label htmlFor="AntecedentesDiag">Antecedentes y/o probable diagnostico</label>
                                    <Controller name="AntecedentesDiag" control={control} render={({ field }) => (
                                        <InputTextarea id={field.name} {...field} placeholder="" autoResize rows="6" cols="30" />
                                    )} />
                                    {/* <InputTextarea id="probDiag" placeholder="" autoResize rows="6" cols="30" /> */}
                                </div>
                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-6">
                                    <label htmlFor="OrganoPrimario">Organo Primario</label>
                                    <Controller name="OrganoPrimario" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={listaDeOrganos} optionLabel="nombreOrgano" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={organoPrimario} onChange={(e) => setOrganoPrimario(e.value)} placeholder="Seleccione" options={Organos} optionLabel="name"></Dropdown> */}
                                    <label htmlFor="OrganoSecundario">Organo Secundario</label>
                                    <Controller name="OrganoSecundario" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={listaDeOrganos} optionLabel="nombreOrgano" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={organoSecundario} onChange={(e) => setOrganoSecundario(e.value)} placeholder="Seleccione" options={Organos} optionLabel="name"></Dropdown> */}
                                </div>
                                <div className="field col-12 md:col-6">
                                    <label htmlFor="PatologoResp">Patólogo Responsable</label>
                                    <Controller name="PatologoResp" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={Patologos} optionLabel="name" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={patologoRes} onChange={(e) => setPatologoRes(e.value)} placeholder="Seleccione" options={Patologos} optionLabel="name"></Dropdown> */}
                                    <label htmlFor="TecnologoResp">Tecnólogo Responsable</label>
                                    <Controller name="TecnologoResp" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={Tecnologos} optionLabel="name" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={tecnologoRes} onChange={(e) => setTecnologoRes(e.value)} placeholder="Seleccione" options={Tecnologos} optionLabel="name"></Dropdown> */}
                                </div>

                                {/* Aca termina el div de 12 */}

                                <div className="field col-12 md:col-12">
                                    <hr />
                                    <h2>Tipo de Examen</h2>
                                </div>
                                {/* Aca termina el div de 12 */}
                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="BiopsiaTipo">Biopsia</label>
                                    <br />
                                    <Controller name="BiopsiaTipo" defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" tooltip="Número de Biopsias" tooltipOptions={{ position: 'bottom' }} />
                                    )} />
                                    {/* <InputNumber buttonLayout="vertical" style={{ width: "3em" }} id="CantMuestra" value={tipoBiopsia} onValueChange={(e) => setTipoBiopsia(e.value)} mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div>
                                    <br />
                                </div>

                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="BiopsiaRapidaTipo">B. Rápida</label>
                                    <br />
                                    <Controller name="BiopsiaRapidaTipo" defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" tooltip="Número de Biopsias Rápidas" tooltipOptions={{ position: 'bottom' }} />
                                    )} />
                                    {/* <InputNumber buttonLayout="vertical" style={{ width: "3em" }} id="CantMuestra" value={tipoBiopRapida} onValueChange={(e) => setTipoBiopRapida(e.value)} mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="CitologiaTipo">Citología</label>
                                    <br />
                                    <Controller name="CitologiaTipo" defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" tooltip="Número de Citologías" tooltipOptions={{ position: 'bottom' }} />
                                    )} />
                                    {/* <InputNumber buttonLayout="vertical" style={{ width: "3em" }} id="CantMuestra" value={citologia} onValueChange={(e) => setCitologia(e.value)} mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="NecropsiaTipo">Necropsia</label>
                                    <br />
                                    <Controller name="NecropsiaTipo" defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" tooltip="Número de Necropsias" tooltipOptions={{ position: 'bottom' }} />
                                    )} />
                                    {/* <InputNumber buttonLayout="vertical" style={{ width: "3em" }} id="CantMuestra" value={necropsia} onValueChange={(e) => setNecropsia(e.value)} mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="PlacasBrTipo">Placas BR</label>
                                    <br />
                                    <Controller name="PlacasBrTipo" fieldValue={0} defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" tooltip="Número de Placas Utilizadas" tooltipOptions={{ position: 'bottom' }} />
                                    )} />
                                    {/* <InputNumber buttonLayout="vertical" style={{ width: "3em" }} id="CantMuestra" value={placasBr} onValueChange={(e) => setPlacasBr(e.value)} mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div className="px-6 field col-12 md:col-2">
                                    <label htmlFor="btnMateriales">Materiales</label>
                                    <br />
                                    <Button id="btnMateriales" type="button" icon={<FaMediumM />} className="p-button-raised" onClick={() => onClick('dialogMateriales')} />
                                </div>

                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-12">
                                    <hr />
                                </div>
                                <div className="field col-12 md:col-12">
                                    <h2>Estado del Informe</h2>
                                </div>
                                {/* Aca termina el div de 12 */}
                                <div className="field col-12 md:col-4">
                                    <label htmlFor="TipoInforme">Tipo de Informe</label>
                                    <Controller name="TipoInforme" control={control} render={({ field }) => (
                                        <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={listFormatoInform} optionLabel="informe" />
                                    )} />
                                    {/* <Dropdown id="PresMuestra" value={tipoInforme} onChange={(e) => setTipoInforme(e.value)} placeholder="Seleccione" options={TiposInforme} optionLabel="name"></Dropdown> */}
                                </div>
                                <div className="field col-12 md:col-2">
                                    {/* <h6>AUGE</h6> */}
                                    <label htmlFor="InformeProcesado">Informe Procesado</label>
                                    <Controller name="InformeProcesado" defaultValue={false} control={control} render={({ field, fieldState }) =>
                                        <ToggleButton id={field.name} {...field} onChange={(e) => field.onChange(e.value)} disabled={!disableFecha1} checked={field.value} onLabel="Si" offLabel="No" />} />
                                    {/* <ToggleButton id="Aug" name="switchValue" checked={toggleEstInforme} onChange={(e) => setToggleInforme(e.value)} onLabel="Si" offLabel="No" /> */}
                                </div>
                                <div className="field col-12 md:col-2">
                                    {/* <h6>AUGE</h6> */}
                                    <label htmlFor="DiasRecep">Días desde recepción</label>
                                    <Controller name="DiasRecep" defaultValue={0} control={control} render={({ field }) => (
                                        <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                    )} />
                                    {/* <InputNumber id="CantMuestra" value={diasRe} onValueChange={(e) => setDiasRe(e.value)} showButtons mode="decimal" min="0"></InputNumber> */}
                                </div>
                                <div className="field col-12 md:col-2">
                                    <label htmlFor="FechaInforme">Fecha Informe</label>
                                    <Controller name="FechaInforme" control={control} render={({ field }) => (
                                        <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" />
                                    )} />
                                    {/* <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fInforme} onChange={(e) => setFInforme(e.value)} locale="es"></Calendar> */}
                                </div>
                                <div className="field col-12 md:col-2">
                                    <label htmlFor="FechaEntInforme">Fecha Entrega Inf.</label>
                                    <Controller name="FechaEntInforme" control={control} render={({ field }) => (
                                        <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" />)} />
                                    {/* <Calendar mask="99/99/9999" dateFormat="dd/mm/yy" showIcon showButtonBar value={fEntregaInforme} onChange={(e) => setFEntregaInforme(e.value)} locale="es"></Calendar> */}
                                </div>

                                <div className="field col-12 md:col-12">
                                    <hr />
                                </div>
                            </div>
                            <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="submit" icon={<FaSave />} className=" p-button-raised p-button-warning" loading={loading3} />
                                </div>
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" style={{ height: 40 }} icon={<img src="assets/demo/images/icons/fonasa32.ico" alt="" />} className="p-button-raised p-button-text" onClick={() => onClick('dialogCodFonasa')} />
                                </div>
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" icon={<FaRegFileAlt />} className="p-button-raised" onClick={(e) => op.current.toggle(e)} disabled={!EstadoInforme} />
                                </div>
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" icon={<GiMicroscope />} className="p-button-raised" onClick={() => onClick('dialogMacroMicro')} />
                                </div>
                                <div className="flex align-items-center justify-content-center w-4rem h-4rem font-bold text-gray-900 border-round m-2">
                                    <Button type="button" icon={<FaStethoscope />} className="p-button-raised" onClick={() => onClick('dialogDiagnostico')} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <Dialog header="Ingrese Macro/Micro" visible={dialogMacroMicro} maximizable modal onHide={() => onHide('dialogMacroMicro')} style={{ width: '50vw' }}>
                <MacroMicro idBiopsia={biopsiaid} />
            </Dialog>
            <Dialog header="Ingrese Codigos Fonasa Utilizados" visible={dialogCodFonasa} maximizable modal onHide={() => onHide('dialogCodFonasa')} style={{ width: '50vw' }}>

                <TablaCodigoFonasa idBiopsia={biopsiaid} />

            </Dialog>
            <Dialog header="DIAGNÓSTICO ANATOMOPATOLÓGICO" visible={dialogDiagnostico} maximizable modal onHide={() => onHide('dialogDiagnostico')} style={{ width: '50vw' }}>
                <Diagnosticos idBiopsia={biopsiaid} />
            </Dialog>
            <Dialog header="Material Exáminado" visible={dialogMateriales} maximizable modal onHide={() => onHide('dialogMateriales')} style={{ width: '50vw' }}>
                <MaterialExaminado idBiopsia={biopsiaid} />
            </Dialog>
            {/* <Dialog header="Informe de Biopsia" visible={dialogInformeBiopsia} maximizable modal onHide={() => onHide('dialogInformeBiopsia')} style={{ width: '50vw' }}>
                <InformeBiopsia Ficha={nroFicha} idBiopsia={biopsiaid} DataPaciente = {person} BiopsiaBus = {biopsiaBuscada} edadPaciente = {edadPersona} fechasBiopsia = {resultBio}/>
            </Dialog> */}

            {/* <Sidebar visible={dialogInformeBiopsia} fullScreen onHide={() => onHide('dialogInformeBiopsia')}>

                    <InformeBiopsia Ficha={nroFicha} idBiopsia={biopsiaid} DataPaciente={person} BiopsiaBus={biopsiaBuscada} edadPaciente={edadPersona} fechasBiopsia={resultBio} />

            </Sidebar> */}
            <OverlayPanel ref={op} showCloseIcon id="overlay_panel" style={{ width: '80px' }} className="overlaypanel-demo">
                <InformeBiopsia Ficha={nroFicha} idBiopsia={biopsiaid} DataPaciente={person} BiopsiaBus={biopsiaBuscada} edadPaciente={edadPersona} fechasBiopsia={resultBio} />
            </OverlayPanel>
            {/* <OverlayPanel ref={op2} showCloseIcon id="overlay_panel" style={{ width: '80px' }} className="overlaypanel-demo">
                <TableBiopsiasPaciente />
            </OverlayPanel> */}
        </React.Fragment>

    );
}
export default FormBuscarBiopsia;

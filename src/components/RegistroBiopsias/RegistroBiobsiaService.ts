import axios from 'axios';

export async function searchPersonaByRut(rut: string) {

    try {
        const response = await axios.get(`http://10.69.206.28:8080/set-api-personas/api/personas/Rut/${rut}`);
        //const response = await axios.get(`http://localhost:8085/api/personas/Rut/${rut}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function saveBiopsia(data: {}){
    try {
        const response = await axios.post(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia`,data);
        return response.data;
    } catch (error) {
    console.log(error);
    return null;

    }
}
export async function updateBiopsia(data: {}, id: string){
    try {
        const response = await axios.put(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/${id}`,data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function searchPrevision() {
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-prevision/api/prevision`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function searchPrevisionByid(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-prevision/api/prevision/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function searchPersonaById(id: string) {
    try {
        const response = await axios.get(`http://10.69.206.28:8080/set-api-personas/api/personas/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}


export async function searchNroFichaById(id: number) {

    try {

        const response = await axios.get(`http://10.69.206.28:8080/set-api-pacientes/api/pacientes/idPersona/${id}`);
        //const response =  await axios.get(`http://localhost:8084/api/pacientes/idPersona/${id}`)
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }


}

export async function searchByNroBiopsia(nroBiopsia: string) {
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/nrobiopsia/${nroBiopsia}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}

export async function searchBiopsiaByIdPaciente(id: string) {
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/persona/${id}`)
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function listServicios(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-servicios/api/servicios`);
        return response.data;
    }  catch (error) {
      console.log(error);
      return null;

    }
}

export async function servicioById(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-servicios/api/servicios/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function formatoMuestra(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-formato-muestra/api/formato`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function listOrganos(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-organos/api/organos`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function listFormatoInforme(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-tipo-informe/api/tipoinforme`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}




import { Button } from "primereact/button";
import { confirmDialog } from "primereact/confirmdialog";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { Toast } from "primereact/toast";
import { classNames } from "primereact/utils";
import React, { useState, useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";
import FormularioBiopsia from "./FormularioBiopsia";
import { saveBiopsia, searchByNroBiopsia } from "./RegistroBiobsiaService";
import Swal from 'sweetalert2'
import FormBuscarBiopsia from "./FormBuscarBiopsia";


const NroBiopsia = ({ InfoPersona }) => {
    const [desplegarFormularioBiopsia, setDesplegarFormularioBiopsia] = useState(false);
    const [desplegarNroBiopsia, setDesplegarNroBiopsia] = useState(true);
    const [validaciones, setValidaciones] = useState(false);
    const [nroBiopsia, setNroBiopsia] = useState("");
    const [idBiopsia, setIdBiopsia] = useState(false);
    const toast = useRef(null);

    const biopsias = {
        nrobiopsia: nroBiopsia,
        auge: "",
        personaId: "",
        medicotratante: "",
        fechatomamuestra: "",
        fecharecepcionmuestra: "",
        servicio: "",
        organo1: "",
        organo2: "",
        patologo: "",
        tecnologo: "",
        dias: "",
        fechainforme: "",
        fechaentregainforme: "",
        biopsia: "",
        biopsiarapida: "",
        citologico: "",
        necropsia: "",
        placas: "",
        muestraenviada: "",
        antecedentes: "",
        tipoinformeid: "",
        formatomuestraid: "",
    };
    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();
    const successAlert = () => {
        Swal.fire({
            title: 'Alerta!',
            text: 'La biopsia ya fue registrada, intente buscar en ya existentes',
            icon: 'warning',
          });
    };
    const submitNroBiopsia = async (data) => {
        console.log("INFOR PERSONA");
        console.log(InfoPersona);
        let validacion = await searchByNroBiopsia(data.nroBiopsia);
        if (validacion) {
            console.log(validacion);
            successAlert();
        } else {
            console.log(data);
            biopsias.nrobiopsia = data.nroBiopsia;
            biopsias.personaId = InfoPersona[0].id;
            let result = await saveBiopsia(biopsias);
            console.log(biopsias.nrobiopsia);
            console.log(biopsias.personaId);
            setNroBiopsia(data.nroBiopsia);
            setDesplegarFormularioBiopsia(!desplegarFormularioBiopsia);
            setDesplegarNroBiopsia(!desplegarNroBiopsia);
            setIdBiopsia(result.id);
            console.log(result);
        }

    }
    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };
    return (
        <React.Fragment>
            <div className={desplegarNroBiopsia ? "show-element" : null}>{
                desplegarNroBiopsia && (
                    <form onSubmit={handleSubmit(submitNroBiopsia)}>
                        <div className="p-grid">
                            <div className="col-12 md:col-12">
                                <div className="card">
                                    <div className="card-header">
                                        <div className="card-title">
                                            <h3>Registrar Número de Biopsia</h3>
                                            <label>Debe ingresar el número de la nueva biopsia</label>
                                            <hr />
                                        </div>
                                        <div className="card-body">
                                            <div className="field col-12 md:col-12">
                                                <div className="field col-12 md:col-5">
                                                    <br />
                                                    <br />
                                                    <div className="p-inputgroup">
                                                        <span className="p-float-label">

                                                            <Controller name="nroBiopsia" control={control} rules={{ required: "El Número de Biopsia es Obligatorio" }} render={({ field, fieldState }) =>
                                                                <InputText id={field.name} {...field} autoFocus className={classNames({ "p-invalid": fieldState.invalid })} />} />
                                                            <label htmlFor="nroBiopsia">Número Biopsia</label>
                                                            {/* <InputText id="NroBiopsia" type="text" /> */}
                                                        </span>
                                                        <Button type="submit" id="pacRutBtn" className="p-button-primary" icon="pi pi-search"/>
                                                    </div>
                                                </div>
                                                {getFormErrorMessage("nroBiopsia")}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                )
            }

            </div>
            <div className={desplegarFormularioBiopsia ? "show-element" : null}>
                {desplegarFormularioBiopsia && (
                    <div className="col-12 md:col-12">
                        <FormBuscarBiopsia nroBiopsia={nroBiopsia} InfoPersona={InfoPersona} idBiopsia={idBiopsia}/>
                        {/* <FormularioBiopsia InfoPersona={InfoPersona} NroDeBiopsia={nroBiopsia} idBiopsia={idBiopsia} /> */}
                    </div>
                )}
            </div>

        </React.Fragment>
    );
}
export default NroBiopsia;

import React, { useState, useEffect, useRef } from "react";
import { Button } from "primereact/button";
import { InputTextarea } from "primereact/inputtextarea";
import { useForm, Controller } from "react-hook-form";
import { classNames } from "primereact/utils";
import { ToggleButton } from 'primereact/togglebutton';
import { saveDiagnostico, searchDiagnosticosById, updateDiagnostico } from "./DiagnosticoService";
import { Toast } from "primereact/toast";




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const Diagnosticos = ({ idBiopsia }) => {
    const [loading1, setLoading1] = useState(false);
    const [diagnosticoId, setDiagnosticoId] = useState("");
    const [Doctor] = useState("Dra. Elena Kakarieka Waaaaaaa aaaaaaaaa");
    const toast = useRef(null);
    const showSuccess = () => {
        toast.current.show({ severity: 'success', summary: 'Guardado', detail: 'La información fue guardada con exito', life: 3000 });
    }
    const showError = () => {
        toast.current.show({severity:'error', summary: 'Error', detail:'Error al guardar la información', life: 3000});
    }
    useEffect(() => {
        search();
    }, []);

    const search = async () => {
        console.log(idBiopsia);
        let biopsiaid = idBiopsia;
        let result = await searchDiagnosticosById(idBiopsia);
        if (!result) {
            let data = {
                "biopsiaid": biopsiaid,
                "notas": "",
                "diagnostico": "",
                "neoplasia": false
            }
            let post = await saveDiagnostico(data);
            setDiagnosticoId(post.id);
            console.log("Post: ");
            console.log(post);
        } else {
            setDiagnosticoId(result.id);
            reset({
                "DiagnosticoAnatomopatológico": result.diagnostico,
                "DescripciónNota": result.notas,
                "Neoplasia": result.neoplasia
            });
            console.log(result);
        }

    }
    const onLoadingClick1 = () => {
        setLoading1(true);
        setTimeout(() => {
            setLoading1(false);
        }, 2000);
    };

    const {
        control,
        handleSubmit,
        reset,
    } = useForm();

    const submitDiagnostico = async (data) => {
        let newData = {
            "biopsiaid": idBiopsia,
            "notas": data.DescripciónNota,
            "diagnostico": data.DiagnosticoAnatomopatológico,
            "neoplasia": data.Neoplasia
        }
        let upd = await updateDiagnostico(newData, diagnosticoId);
        if(upd){
            showSuccess();
        }
        else{
            showError();
        }
        console.log(data);
        console.log(upd);
        onLoadingClick1();
    };


    //////////////////////////                                           //////////////////////////////                        ///////////////////////////////

    return (


        <form onSubmit={handleSubmit(submitDiagnostico)}>
            <div className="p-grid">
                <div className="col-12">
                    <div className="p-fluid formgrid grid">
                    <Toast ref={toast} position="bottom-center"/>


                        <div className="field col-12 md:col-12">
                            <Controller
                                name="DiagnosticoAnatomopatológico"
                                control={control}
                                render={({ field, fieldState }) => (
                                    <InputTextarea tooltipOptions placeholder="Ingresar la Descripción del Diagnostico Anatomopatológico" id={field.name} value={field.value}   {...field} autoResize rows="15" cols="20" className={classNames({ "p-invalid": fieldState.invalid })} />
                                )}
                            />
                        </div>

                        <div className="field col-12 md:col-12">
                            <h3>Nota</h3>
                            <Controller
                                name="DescripciónNota"
                                control={control}
                                render={({ field, fieldState }) => <InputTextarea placeholder=" Ingresar la Descripción Nota" id={field.name} value={field.value}  {...field} autoResize rows="8" cols="20" className={classNames({ "p-invalid": fieldState.invalid })} />}
                            />
                        </div>
                        {/* <Controller name="nroBiopsia" control={control} rules={{ required: "El Nro de Biopsia es Obligatorio" }} render={({ field, fieldState }) =>
                                            <InputText id={field.name} {...field} autoFocus className={classNames({ "p-invalid": fieldState.invalid })} />} />/ */}
                        <br />


                        <div className="field col-12 md:col-4">
                            <h4>Patológo Responsable</h4>
                            <hr></hr>
                            <h3>   {Doctor}</h3>
                        </div>



                        <div className="pt-6 flex-2 col-12 md:col-4">

                            <Button type="submit" label="Ingresar Observación" icon="FaRegFileAlt" className="mr-2 mb-2" loading={loading1} position="bottom-right" />
                        </div>
                        <div className="field col-12 md:col-1 col-offset-1">
                            <label htmlFor="Neoplasia">Neoplasia</label>
                            <Controller name="Neoplasia" defaultValue={false} control={control} render={({ field }) =>
                                <ToggleButton id={field.name} {...field} onChange={(e) => field.onChange(e.value)} checked={field.value} onLabel="Si" offLabel="No" />} />

                        </div>
                    </div>
                </div>
            </div>
        </form>




    );
};
export default Diagnosticos;

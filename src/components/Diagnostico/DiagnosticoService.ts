import axios from "axios";

export async function searchDiagnosticosById(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-diagnostico/api/diagnostico/biopsia/${id}`);

        return response.data;
      } catch (err) {
        console.error(err);
        return null;
      }
}

export async function saveDiagnostico(data:{}){
    try {
        const response = await axios.post("http://10.69.206.32:8080/api-apa-diagnostico/api/diagnostico", data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function updateDiagnostico(data:{},id:string){
    try {
        const response = await axios.put(`http://10.69.206.32:8080/api-apa-diagnostico/api/diagnostico/${id}`, data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}















import React, { useState, useEffect, useRef } from "react";
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Editor } from 'primereact/editor';
import { FaSave } from "@react-icons/all-files/fa/FaSave";
import { FaRegFileAlt } from "@react-icons/all-files/fa/FaRegFileAlt";
import { searchMacroMicro, updateMacroMicro, saveMacroMicros } from "./MacroMicroService";
const MacroMicro = ({ idBiopsia }) => {
    const [activeIndexCodFonasa, setActiveIndexCodFonasa] = useState(0);
    const [text1, setText1] = useState('');
    const [text2, setText2] = useState('');
    const [loading1, setLoading1] = useState(false);
    const [macroMicroId, setMacroMicroId] = useState(null);
    const toast = useRef(null);

    useEffect(() => {
        searchMacMic();
    }, []);
    const onLoadingClick1 = () => {
        setLoading1(true);
        setTimeout(() => {
            setLoading1(false);
        }, 2000);
    };
    const searchMacMic = async () => {
        let id = idBiopsia;
        let result = await searchMacroMicro(id);
        if (!result) {
            let data = {
                "biopsiaId": idBiopsia,
                "macroscopia": "",
                "microscopia": ""
            }
            let post = await saveMacroMicros(data);
            setText1(post.macroscopia);
            setText2(post.microscopia);
            setMacroMicroId(post.id);
            console.log(post);
        } else {
            setMacroMicroId(result.id);
            setText1(result.macroscopia);
            setText2(result.microscopia);
            console.log(result);
        }


    };
    const renderHeader = () => {
        return (
            <span className="ql-formats">
                <button className="ql-bold" aria-label="Bold"></button>
                <button className="ql-italic" aria-label="Italic"></button>
                <button className="ql-underline" aria-label="Underline"></button>
            </span>
        );
    }
    const header = renderHeader();
    const showSuccess = () => {
        toast.current.show({ severity: 'success', summary: 'Guardado', detail: 'La información fue guardada con exito', life: 3000 });
    }
    const showError = () => {
        toast.current.show({ severity: 'error', summary: 'Error', detail: 'Error al guardar la información', life: 3000 });
    }
    const saveMacroMicro = async () => {
        let newValue = text1;
        let newValue2 = text2;
        if(newValue === undefined || newValue === null){
            newValue = '';
        }
        if(newValue2 === undefined || newValue2 === null){
            newValue2 = '';
        }
        console.log(newValue);
        let data = {
            "biopsiaId": idBiopsia,
            "macroscopia": newValue,
            "microscopia": newValue2
        }
        console.log(data);
        onLoadingClick1();
        let mac = await updateMacroMicro(macroMicroId, data);
        if (mac) {
            showSuccess();
        } else {
            showError();
        }
    }

    const exportPdf = () => {
        let data = {
            "macro": text1,
            "micro": text2,
        }
        var DatosPaciente = {
            Nombre: 'Roger Salvo',
            Edad: '85a',
            Rut: '999999-9',
            NroFicha: '99999',
            FechaRecepcion: '03/08/2022',
            SolicitadoPor: 'Francisco Carpintero',
            OrganoPrimario: 'Cólon'
        }
        var fontSizes = {
            HeadTitleFontSize: 18,
            Head2TitleFontSize: 16,
            TitleFontSize: 14,
            SubTitleFontSize: 12,
            NormalFontSize: 10,
            SmallFontSize: 8
        };
        var lineSpacing = {
            NormalSpacing: 12,
        };
        import('jspdf').then(jsPDF => {
            const doc = new jsPDF.default('p', 'pt');
            console.log(doc.getFontList());
            const docWidth = doc.internal.pageSize.getWidth();
            const docHeight = doc.internal.pageSize.getHeight();
            var rightStartCol1 = 400;
            var rightStartCol2 = 480;
            var InitialstartX = 40;
            var startX = 40;
            var InitialstartY = 50;
            var startY = 0;
            var lineHeights = 12;
            var encabezado = 'Servicio de Salud Metropolitano Central\n Complejo de Salud San Borja Arriarán\n      Unidad de Anatomía Patológica';
            doc.setFontSize(fontSizes.HeadTitleFontSize);
            const splitDescription = doc.splitTextToSize(
                encabezado,
                docWidth - 20
            );
            doc.text(splitDescription, 135, 20);
            doc.line(0, 70, docWidth, 70);
            doc.setFont("helvetica", "italic");
            //doc.setFontStyle('italic');
            //doc.text(DatosPaciente.Nombre, 80, startY);
            //(method) jsPDF.output(type: "dataurlnewwindow" | "pdfobjectnewwindow" | "pdfjsnewwindow", options?: {

            doc.output({ filename: 'pdfTest' });
            //doc.save('InvoiceTemplate.pdf');

        })


        //quita las etiquetas html
        let strippedHtml = text1.replace(/<[^>]+>/g, '');
        // import('jspdf').then(jsPDF => {
        //     import('jspdf-autotable').then(() => {
        //         const doc = new jsPDF.default(0, 0);
        //         doc.setFontSize(40);
        //         doc.text(40,20,strippedHtml);
        //         doc.save('products.pdf');
        //     })
        // })
    }


    return (
        <React.Fragment>
            <div>

                <TabView activeIndex={activeIndexCodFonasa} onTabChange={(e) => setActiveIndexCodFonasa(e.index)}>

                    <TabPanel header="Macroscopía">
                        <Toast ref={toast} position="bottom-right" />
                        <ConfirmDialog />
                        <div className="field col-12 md:col-12">
                            <div className="field col-12 md:col-12">
                                <h3>Descripción de Macroscopía</h3>
                                <hr />
                            </div>
                            <div className="field col-12 md:col-12">
                                <Editor maxLength={250} headerTemplate={header} style={{ height: '320px' }} value={text1} onTextChange={(e) => setText1(e.htmlValue)} />
                            </div>
                            {/* <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                                <div className="px-1">
                                    <Button type="button" icon={<FaSave />} className=" p-button-raised p-button-warning" onClick={() => saveMacroMicro()} />
                                </div>
                                <div className="px-1">
                                    <Button type="button" icon={<FaRegFileAlt />} className="p-button-raised" onClick={() => exportPdf()} />
                                </div>
                            </div> */}
                        </div>
                    </TabPanel>
                    <TabPanel header="Microscopía">
                        <Toast ref={toast} position="bottom-right" />
                        <ConfirmDialog />
                        <div className="field col-12 md:col-12">
                            <div className="field col-12 md:col-12">
                                <h3>Descripción de Microscopía</h3>
                                <hr />
                            </div>
                            <div className="field col-12 md:col-12">
                                <Editor headerTemplate={header} style={{ height: '320px' }} value={text2} onTextChange={(e) => setText2(e.htmlValue)} />
                            </div>

                        </div>
                    </TabPanel>
                </TabView>
                <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                    <div className="px-1">
                        <Button type="button" icon={<FaSave />} className=" p-button-raised p-button-warning" onClick={() => saveMacroMicro()} loading={loading1} />
                    </div>
                    <div className="px-1">
                        <Button type="button" icon={<FaRegFileAlt />} className="p-button-raised" onClick={() => exportPdf()} />
                    </div>
                </div>
            </div>
        </React.Fragment>
    )

};
export default MacroMicro;

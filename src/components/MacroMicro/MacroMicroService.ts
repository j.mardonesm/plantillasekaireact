import axios from 'axios';

export async function searchMacroMicro(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-macro-micro/api/macromicro/${id}`);
        return response.data;

    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function saveMacroMicros(data:{}){
    try {
        const response = await axios.post(`http://10.69.206.32:8080/api-apa-macro-micro/api/macromicro`, data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
        }

}

export async function updateMacroMicro(id: string, data:{}){
    try {
        console.log(data);
        const response = await axios.put(`http://10.69.206.32:8080/api-apa-macro-micro/api/macromicro/${id}`, data);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }

}


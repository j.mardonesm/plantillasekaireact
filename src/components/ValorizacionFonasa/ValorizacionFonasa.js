import React, { useState, useEffect, useRef } from "react";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { InputNumber } from "primereact/inputnumber";
import { Calendar } from "primereact/calendar";
import { CountryService } from "../../service/CountryService";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Sidebar } from "primereact/sidebar";
import { NodeService } from "../../service/NodeService";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { CustomerService } from "../../service/CustomerService";
import { ProductService } from "../../service/ProductService";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { classNames } from "primereact/utils";
import { FileUpload } from "primereact/fileupload";
import { Rating } from "primereact/rating";
import { RadioButton } from "primereact/radiobutton";
import { Toolbar } from "primereact/toolbar";
import { Toast } from "primereact/toast";
import { RutPersona, SearchClinicos, CodFonasaService, CodFonasa, BiopsiaByFecha,listServicios } from "./ValorizacionFonasaService";
import { Dropdown } from "primereact/dropdown";
import { useForm, Controller } from "react-hook-form";
import { locale, addLocale, updateLocaleOption, updateLocaleOptions, localeOption, localeOptions } from "primereact/api";
import Swal from "sweetalert2";
import { searchPersonaById } from "../RegistroBiopsias/RegistroBiobsiaService";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
locale("es");
addLocale("es", {
    firstDayOfWeek: 1,
    dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
    today: "Hoy",
    clear: "Limpiar",
});

const InvalidStateDemo = () => {
    let emptyProduct = {
        id: null,
        name: "",
        image: null,
        description: "",
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: "INSTOCK",
    };

    const [products, setProducts] = useState(null);
    const [productDialog, setProductDialog] = useState(false);
    const [deleteProductDialog, setDeleteProductDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [selectedProducts, setSelectedProducts] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);
    const [servClinico, setServClinico] = useState([]);
    const [sum1001, setsum1001] = useState(0);
    const [sum1002, setsum1002] = useState(0);
    const [sum1003, setsum1003] = useState(0);
    const [sum1004, setsum1004] = useState(0);
    const [sum1005, setsum1005] = useState(0);
    const [sum1006, setsum1006] = useState(0);
    const [sum1007, setsum1007] = useState(0);
    const [sum1008, setsum1008] = useState(0);
    const [sum1009, setsum1009] = useState(0);
    const [sum1010, setsum1010] = useState(0);
    const [servicios, setServicios] = useState([]);
    const [personaTable, setPersonaTable] = useState([]);


        useEffect(() => {
            search();
        }, []);

    const search = async () => {
        let result = await listServicios();
        setServicios(result);
        console.log(result);
    };

    const Patologos = [
        {
            id: 1,
            name: "Dr. Francisco Carpintero",
        },
        {
            id: 2,
            name: "Dr. Fernando Lopez",
        },
        {
            id: 3,
            name: "Dr. Juan Carlos Paz",
        },
        {
            id: 4,
            name: "Dra. Maria de los Angeles",
        },
        {
            id: 5,
            name: "Dra. Josefa Ramirez",
        },
        {
            id: 6,
            name: "Dr. Juan Carlos Manuel Mardones Montiel",
        },
    ];
    

    const ServiciosPat = [
        {
            id: 1,
            ServicioClinico: "Patologia Mamaria",
            ApellidoPaterno: "Mardones",
            ApellidoMaterno: "Montiel",
            1001: "1",
            1002: "2",
            1003: "1",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 2,
            ServicioClinico: "Ginecologia General",
            ApellidoPaterno: "Salvo ",
            ApellidoMaterno: "Muñoz",
            1001: "1",
            1002: "1",
            1003: "2",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 3,
            ServicioClinico: "Piso Pelvico",
            ApellidoPaterno: "Carpintero",
            ApellidoMaterno: "Caceres",
            1001: "1",
            1002: "1",
            1003: "1",
            1004: "3",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 4,
            ServicioClinico: "Oncología Pelvica",
            ApellidoPaterno: "Lopez",
            ApellidoMaterno: "Delgado",
            1001: "1",
            1002: "2",
            1003: "1",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 5,
            ServicioClinico: "Patologia Mamaria",
            ApellidoPaterno: "Venegas",
            ApellidoMaterno: "Perez",
            1001: "1",
            1002: "2",
            1003: "3",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 6,
            ServicioClinico: "Turno de Residencia",
            ApellidoPaterno: "Muñoz ",
            ApellidoMaterno: "Avaloz",
            1001: "1",
            1002: "2",
            1003: "3",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 7,
            ServicioClinico: "Obstetricia",
            ApellidoPaterno: "Salvo ",
            ApellidoMaterno: "Contreras",
            1001: "1",
            1002: "2",
            1003: "3",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
        {
            id: 8,
            ServicioClinico: "Parto",
            ApellidoPaterno: "Sanchez ",
            ApellidoMaterno: "Ugarte",
            1001: "1",
            1002: "2",
            1003: "3",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },

        {
            id: 9,
            ServicioClinico: "Pre-Parto",
            ApellidoPaterno: "Valdivia ",
            ApellidoMaterno: "De la Hoya",
            1001: "1",
            1002: "2",
            1003: "3",
            1004: "1",
            1005: "2",
            1006: "1",
            1007: "2",
            1008: "1",
            1009: "2",
            1010: "1",
        },
    ]; 

    async function renderizado(result) {
      
        var sumCollection = [];
        var personaCollection = [];
        await Promise.all(

            result.map(async (element) => {
            let sum1001 = 0;
            let sum1002 = 0;
            let sum1003 = 0;
            let sum1004 = 0;
            let sum1005 = 0;
            let sum1006 = 0;
            let sum1007 = 0;
            let sum1008 = 0;
            let sum1009 = 0;
            let sum1010 = 0;

            let persona = await searchPersonaById(element.personaId);
            let sum = await CodFonasa(element.id);
            for (var el in sum) {
                switch (sum[el].codigo) {
                    case 801001:
                        sum1001 = sum1001 + sum[el].cantidad;
                        break;
                    case 801002:
                        sum1002 = sum1002 + sum[el].cantidad;
                        break;
                    case 801003:
                        sum1003 = sum1003 + sum[el].cantidad;
                        break;
                    case 801004:
                        sum1004 = sum1004 + sum[el].cantidad;
                        break;
                    case 801005:
                        sum1005 = sum1005 + sum[el].cantidad;
                        break;
                    case 801006:
                        sum1006 = sum1006 + sum[el].cantidad;
                        break;
                    case 801007:
                        sum1007 = sum1007 + sum[el].cantidad;
                        break;
                    case 801008:
                        sum1008 = sum1008 + sum[el].cantidad;
                        break;
                    case 801009:
                        sum1009 = sum1009 + sum[el].cantidad;
                        break;
                    case 801010:
                        sum1010 = sum1010 + sum[el].cantidad;
                        break;
                    default:
                        break;
                }
            }

            let arr = {
                "id": element.id,
                "nombre": persona.primerNombre,
                "Apaterno": persona.apellidoPaterno,
                "AMaterno": persona.apellidoMaterno,
                "Rut": persona.nroDocumento,
                "NroBiopsia": element.nrobiopsia,
                "1001": sum1001,
                "1002":sum1002,
                "1003":sum1003,
                "1004":sum1004,
                "1005":sum1005,
                "1006":sum1006,
                "1007":sum1007,
                "1008":sum1008,
                "1009":sum1009,
                "1010":sum1010,
            };
            personaCollection = [...personaCollection,arr];

            sumCollection = [...sumCollection, element.id];

        })
        );
       
        console.log(personaCollection);
        setPersonaTable(personaCollection);
        // await Promise.all(
        //     sumCollection.map(async (e) => {
        //         let sum = await CodFonasa(e);
        //         //console.log(sum.codigo);

        //         for (var el in sum) {
        //             switch (sum[el].codigo) {
        //                 case 801001:
        //                     sum1001 = sum1001 + sum[el].cantidad;
        //                     break;
        //                 case 801002:
        //                     sum1002 = sum1002 + sum[el].cantidad;
        //                     break;
        //                 case 801003:
        //                     sum1003 = sum1003 + sum[el].cantidad;
        //                     break;
        //                 case 801004:
        //                     sum1004 = sum1004 + sum[el].cantidad;
        //                     break;
        //                 case 801005:
        //                     sum1005 = sum1005 + sum[el].cantidad;
        //                     break;
        //                 case 801006:
        //                     sum1006 = sum1006 + sum[el].cantidad;
        //                     break;
        //                 case 801007:
        //                     sum1007 = sum1007 + sum[el].cantidad;
        //                     break;
        //                 case 801008:
        //                     sum1008 = sum1008 + sum[el].cantidad;
        //                     break;
        //                 case 801009:
        //                     sum1009 = sum1009 + sum[el].cantidad;
        //                     break;
        //                 case 801010:
        //                     sum1010 = sum1010 + sum[el].cantidad;
        //                     break;
        //                 default:
        //                     break;
        //             }
        //         }
        //     })
        // );
        var collection = [
            {
                sum1001: sum1001,
                sum1002: sum1002,
                sum1003: sum1003,
                sum1004: sum1004,
                sum1005: sum1005,
                sum1006: sum1006,
                sum1007: sum1007,
                sum1008: sum1008,
                sum1009: sum1009,
                sum1010: sum1010,
            },
        ];
        return collection;
        // }, );
    }

    //Inicialización de función para llamar datos de un api
    const Buscar = async () => {
        let result = await SearchClinicos();
        let arr = Object.assign(servClinico, result);
        /* console.log(result);
         setServClinico([
             ...servClinico,
             result
         ]);*/
    };

    useEffect(() => {
        Buscar();
    }, []);

    useEffect(() => {
        const productService = new ProductService();
        productService.getProducts().then((data) => setProducts(data));
    }, []);
    

    // const formatCurrency = (value) => {
    //     return value.toLocaleString("en-US", { style: "currency", currency: "USD" });
    // };

    const openNew = () => {
        setProduct(emptyProduct);
        setSubmitted(false);
        setProductDialog(true);
    };

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    };

    const hideDeleteProductDialog = () => {
        setDeleteProductDialog(false);
    };

    const hideDeleteProductsDialog = () => {
        setDeleteProductsDialog(false);
    };

    const saveProduct = () => {
        setSubmitted(true);
        if (product.name.trim()) {
            let _products = [...products];
            let _product = { ...product };
            if (product.id) {
                const index = findIndexById(product.id);

                _products[index] = _product;
                toast.current.show({ severity: "success", summary: "Successful", detail: "Product Updated", life: 3000 });
            } else {
                _product.id = createId();
                _product.image = "product-placeholder.svg";
                _products.push(_product);
                toast.current.show({ severity: "success", summary: "Successful", detail: "Product Created", life: 3000 });
            }
            setProducts(_products);
            setProductDialog(false);
            setProduct(emptyProduct);
        }
    };

   

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                index = i;
                break;
            }
        }
        return index;
    };

    const createId = () => {
        let id = "";
        let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    };

    const exportCSV = () => {
        dt.current.exportCSV();
    };

    const exportColumns = ServiciosPat.map((col) => ({ title: col.header, dataKey: col.field }));

    const saveAsExcelFile = (buffer, fileName) => {
        import("file-saver").then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
                let EXCEL_EXTENSION = ".xlsx";
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE,
                });

                module.default.saveAs(data, fileName + "_export_" + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const exportExcel = () => {
        import("xlsx").then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(ServiciosPat);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
            const excelBuffer = xlsx.write(workbook, { bookType: "xlsx", type: "array" });
            saveAsExcelFile(excelBuffer, "ServiciosPat");
        });
    };

   

    const deleteSelectedProducts = () => {
        let _products = products.filter((val) => !selectedProducts.includes(val));
        setProducts(_products);
        setDeleteProductsDialog(false);
        setSelectedProducts(null);
        toast.current.show({ severity: "success", summary: "Successful", detail: "Products Deleted", life: 3000 });
    };

    const onCategoryChange = (e) => {
        let _product = { ...product };
        _product["category"] = e.value;
        setProduct(_product);
    };

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || "";
        let _product = { ...product };
        _product[`${name}`] = val;

        setProduct(_product);
    };

    const onInputNumberChange = (e, name) => {
        const val = e.value || 0;
        let _product = { ...product };
        _product[`${name}`] = val;

        setProduct(_product);
    };

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                </div>
            </React.Fragment>
        );
    };
    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                {/* <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" /> */}
                <Button label="CSV" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
                <Button label="EXCEL" icon="pi pi-upload" className="p-button-help" onClick={exportExcel} />
            </React.Fragment>
        );
    };
    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Descripción:</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );
    const productDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveProduct} />
        </>
    );
    const deleteProductDialogFooter = (
        <>
        </>
    );
    const deleteProductsDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteSelectedProducts} />
        </>
    );

    const successAlert = () => {
        Swal.fire({
            title: "Alerta!",
            text: "Busqueda sin resultados",
            icon: "warning",
        });
    };

    const [visibleFullScreen, setVisibleFullScreen] = useState(false);

    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();

    const submitBiop = async (data) => {

        console.log(data);
        let servicioId 
        let patologosId 
        if (data.ServiciosOrigen === undefined)
        {
            servicioId = 0;
            
        }else{
            servicioId = data.ServiciosOrigen.id;
        };
        if (data.Patologos === undefined)
        {
            patologosId = 0;
            
        }else{
            patologosId = data.Patologos.id;
        }


        let desde;
        let hasta;

        desde = data.FechaDesde;
        hasta = data.FechaHasta;

        let desdeFormateada = desde;
        let hastaFormateada = hasta;
        
        desde = new Intl.DateTimeFormat("fr-CA").format(desdeFormateada);
        hasta = new Intl.DateTimeFormat("fr-CA").format(hastaFormateada);



        let result = await BiopsiaByFecha(desde, hasta);

        // sumas = await renderizado(arreglo);
            var arreglo = [];
                let sumas;
        if (data.ServiciosOrigen === undefined && data.Patologos === undefined)  {
            sumas = await renderizado(result);
        } 
        
        else if (data.ServiciosOrigen && data.Patologos === undefined) {
            result.map(async (element) => {
                if (element.servicio === servicioId ) {
                    
                    arreglo = [...arreglo, element];
                }
            });
            if (arreglo.length > 0) {
                sumas = await renderizado(arreglo);
            } else {
                return successAlert();
            }
        }else if(data.ServiciosOrigen === undefined && data.Patologos )
        {
            result.map(async (element) => {
                if (element.patologo === patologosId ) {
                    
                    arreglo = [...arreglo, element];
                }
            });
            if (arreglo.length > 0) {
                sumas = await renderizado(arreglo);
            } else {
                return successAlert();
            }
        }else if(data.ServiciosOrigen && data.Patologos)
        {
            result.map(async (element) => {
                if (element.servicio === servicioId && element.patologo === patologosId) {
                    
                    arreglo = [...arreglo, element];
                }
            });
            if (arreglo.length > 0) {
                sumas = await renderizado(arreglo);
            } else {
                return successAlert();
            }
        }
        if (arreglo.length === 0) {
        } else {
        }
         console.log(arreglo);
        setsum1001(sumas[0].sum1001);
        setsum1002(sumas[0].sum1002);
        setsum1003(sumas[0].sum1003);
        setsum1004(sumas[0].sum1004);
        setsum1005(sumas[0].sum1005);
        setsum1006(sumas[0].sum1006);
        setsum1007(sumas[0].sum1007);
        setsum1008(sumas[0].sum1008);
        setsum1009(sumas[0].sum1009);
        setsum1010(sumas[0].sum1010);        

        
        console.log(sumas);
        
        setVisibleFullScreen(true);
        reset();
    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };

    return (

        <div className=" p-grid p-fluid ">
            <div className=" card ">
                <h5>Listado Codigos Fonasa</h5>
                <h5>Ingrese un Rango De Fecha</h5>
                <br></br>
                <form onSubmit={handleSubmit(submitBiop)}>
                    <div className=" flex   col-12 md:col-12 ">
                        <div className="    col-6 md:col-6 ">
                            <label htmlFor="FechaDesde">DESDE</label>
                            <Controller
                                name="FechaDesde"
                                control={control}
                                rules={{ required: "Debe Ingresar un Rango de Fecha Inicial" }}
                                render={({ field, fieldState }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" disabledDays={[0, 6]} readOnlyInput className={classNames({ "p-invalid": fieldState.invalid })} />
                                )}
                            />
                            {getFormErrorMessage("FechaDesde")}
                        </div>
                        <div className="  col-6 md:col-6 ">
                            <label htmlFor="FechaHasta">HASTA</label>
                            <Controller
                                name="FechaHasta"
                                control={control}
                                rules={{ required: "Debe Ingresar un Rango de Fecha Limite" }}
                                render={({ field, fieldState }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" disabledDays={[0, 6]} readOnlyInput className={classNames({ "p-invalid": fieldState.invalid })} />
                                )}
                            />
                            {getFormErrorMessage("FechaHasta")}
                        </div>
                    </div>
                    <div className=" flex   col-12 md:col-12">
                        <div className="   col-6 md:col-6 ">
                            <label htmlFor="Edad">SERVICIO</label>
                            <Controller name="ServiciosOrigen" control={control} render={({ field }) => <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={servicios} optionLabel="descripcion" />} />
                        </div>
                        <div className="   col-6 md:col-6">
                            <label htmlFor="Edad">PATÓLOGO</label>
                            <Controller name="Patologos" control={control} render={({ field }) => <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={Patologos} optionLabel="name" />} />
                        </div>
                        <br></br> 
                        <br></br> 
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                    </div>
                    <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                        <Button label="Listado Códigos Fonasa" className="mr-2 mb-2" />
                    </div>
                    <br></br>
                    <br></br>
                </form>
                <Sidebar visible={visibleFullScreen} onHide={() => setVisibleFullScreen(false)} baseZIndex={1000} fullScreen>
                    <h1>Listado Códigos De Fonasa</h1>
                    <div className="grid">
                        <div className="col-12">
                            <div className="card">
                                <h5>Listado De Codigos</h5>
                                <div className="col-12">
                                    <div className="card">
                                        <Toast ref={toast} />
                                        <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>
                                        <DataTable 
                                            ref={dt}
                                            value={personaTable}
                                            selection={selectedProducts}
                                            onSelectionChange={(e) => setSelectedProducts(e.value)}
                                            dataKey="table-excel"
                                            
                                            selectionMode="multiple"
                                            rowsPerPageOptions={[5, 10, 25]}
                                            className="datatable-responsive"
                                            paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                            currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                                            globalFilter={globalFilter}
                                            emptyMessage="No products found."
                                            header={header}
                                            responsiveLayout="scroll"
                                        >
                                            <Column selectionMode="multiple" ></Column>
                                            <Column field="NroBiopsia" header="NroBiopsia" sortable ></Column>
                                           
                                             <Column field="Rut" header="Rut" sortable ></Column>

                                            <Column field="nombre" header="Nombre" sortable ></Column>
                                            <Column field="AMaterno" header="AMaterno" sortable ></Column>
                                            <Column field="Apaterno" header="Apaterno" sortable ></Column>
                                            <Column field="1001" header="1001" sortable ></Column>
                                            <Column field="1002" header="1002" sortable ></Column>
                                            <Column field="1003" header="1003" sortable ></Column>
                                            <Column field="1004" header="1004" sortable ></Column>
                                            <Column field="1005" header="1005" sortable ></Column>
                                            <Column field="1006" header="1006" sortable ></Column>
                                            <Column field="1007" header="1007" sortable ></Column>
                                            <Column field="1008" header="1008" sortable ></Column>
                                            <Column field="1009" header="1009" sortable ></Column>
                                            <Column field="1010" header="1010" sortable ></Column>
                                        </DataTable>
                                        <Dialog visible={productDialog} style={{ width: "450px" }} header="Product Details" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                                            {product.image && <img src={`assets/demo/images/product/${product.image}`} alt={product.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                                            <div className="field">
                                                <label htmlFor="name">Name</label>
                                                <InputText id="name" value={product.name} onChange={(e) => onInputChange(e, "name")} required autoFocus className={classNames({ "p-invalid": submitted && !product.name })} />
                                                {submitted && !product.name && <small className="p-invalid">Name is required.</small>}
                                            </div>
                                            <div className="field">
                                                <label htmlFor="description">Description</label>
                                                <InputTextarea id="description" value={product.description} onChange={(e) => onInputChange(e, "description")} required rows={3} cols={20} />
                                            </div>
                                            <div className="field">
                                                <label className="mb-3"></label>
                                                <div className="formgrid grid">
                                                    <div className="field-radiobutton col-6">
                                                        <RadioButton inputId="category1" name="category" value="Accessories" onChange={onCategoryChange} checked={product.category === "Accessories"} />
                                                        <label htmlFor="category1"></label>
                                                    </div>
                                                    <div className="field-radiobutton col-6">
                                                        <RadioButton inputId="category2" name="category" value="Clothing" onChange={onCategoryChange} checked={product.category === "Clothing"} />
                                                        <label htmlFor="category2"></label>
                                                    </div>
                                                    <div className="field-radiobutton col-6">
                                                        <RadioButton inputId="category3" name="category" value="Electronics" onChange={onCategoryChange} checked={product.category === "Electronics"} />
                                                        <label htmlFor="category3"></label>
                                                    </div>
                                                    <div className="field-radiobutton col-6">
                                                        <RadioButton inputId="category4" name="category" value="Fitness" onChange={onCategoryChange} checked={product.category === "Fitness"} />
                                                        <label htmlFor="category4"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="formgrid grid">
                                                <div className="field col">
                                                    <label htmlFor="price"></label>
                                                    <InputNumber id="price" value={product.price} onValueChange={(e) => onInputNumberChange(e, "price")} mode="currency" currency="USD" locale="en-US" />
                                                </div>
                                                <div className="field col">
                                                    <label htmlFor="quantity"></label>
                                                    <InputNumber id="quantity" value={product.quantity} onValueChange={(e) => onInputNumberChange(e, "quantity")} integeronly />
                                                </div>
                                            </div>
                                        </Dialog>
                                        <Dialog visible={deleteProductDialog} style={{ width: "450px" }} header="Confirm" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
                                            <div className="flex align-items-center justify-content-center">
                                                <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: "2rem" }} />
                                                {product && (
                                                    <span>
                                                        Are you sure you want to delete <b>{product.name}</b>?
                                                    </span>
                                                )}
                                            </div>
                                        </Dialog>
                                        <Dialog visible={deleteProductsDialog} style={{ width: "450px" }} header="Confirm" modal footer={deleteProductsDialogFooter} onHide={hideDeleteProductsDialog}>
                                            <div className="flex align-items-center justify-content-center">
                                                <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: "2rem" }} />
                                                {product && <span>Are you sure you want to delete the selected products?</span>}
                                            </div>
                                        </Dialog>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Sidebar>
                {/* <Button type="button" icon="pi pi-external-link" className="p-button-warning" onClick={() => setVisibleFullScreen(true)} /> */}
            </div>
        </div>
    );
};

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(InvalidStateDemo, comparisonFn);















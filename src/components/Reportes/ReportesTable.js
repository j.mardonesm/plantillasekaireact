import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { Tooltip } from "primereact/tooltip";
import React, { useState, useEffect, useRef } from "react";
import { searchBiopsiaByid, searchNeoplasia } from "./ReportesService";
import { useDownloadExcel } from "react-export-table-to-excel";
import { DomHandler, ObjectUtils } from "primereact/utils";
import ExportJsonExcel from 'js-export-excel';

const ReportesTable = ({ Campos }) => {
    const [cols, setCols] = useState([]);
    const [tableBiopsia, setTableBiopsia] = useState([]);
    const [selectedProducts, setSelectedProducts] = useState([]);
    const dt = useRef(null);
    const toast = useRef(null);
    const tableRef = useRef(null);
    useEffect(() => {
        Columns();
    }, []);
    const Columns = async () => {
        let neo = await searchNeoplasia();
        let result;
        let data = [];
        neo.forEach(async (element) => {
            let search = await searchBiopsiaByid(element.biopsiaid);
            data = [...data, search];
            setTableBiopsia(data);
            console.log(data);
            for (var name in search) {
                for (var cam in Campos) {
                    if (name === Campos[cam].name) {
                        if (cols.length < Campos.length) {
                            cols.push({
                                id: cam,
                                label: Campos[cam].label,
                                name: Campos[cam].name
                            });
                            // data.push({
                            //     id : cam,
                            //     label: Campos[cam].label,
                            //     name : name,
                            // });

                            console.log(name);
                        }


                    }

                }

            }

        });
        console.log(data);
        console.log(cols);
        console.log(result);
        console.log(neo);
        console.log(Campos);
    };
    const { onDownload } = useDownloadExcel({
        currentTableRef: tableRef.current,
        filename: "Users table",
        sheet: "tableBiopsia"
    });
    function _arrayLikeToArray(arr, len) {
        if (len == null || len > arr.length) len = arr.length;

        for (var i = 0, arr2 = new Array(len); i < len; i++) {
            arr2[i] = arr[i];
        }

        return arr2;
    }
    function _unsupportedIterableToArray(o, minLen) {
        if (!o) return;
        if (typeof o === "string") return _arrayLikeToArray(o, minLen);
        var n = Object.prototype.toString.call(o).slice(8, -1);
        if (n === "Object" && o.constructor) n = o.constructor.name;
        if (n === "Map" || n === "Set") return Array.from(o);
        if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
    }
    function _nonIterableSpread() {
        throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    function _iterableToArray(iter) {
        if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
    }
    function _toConsumableArray(arr) {
        return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
    }
    function _arrayWithoutHoles(arr) {
        if (Array.isArray(arr)) return _arrayLikeToArray(arr);
    }
    const exportExcel = (options) => {
        var headers = [];
        var bodys = [];
        var excel = [];
        var data = tableRef.current;
        console.log(data);
        var csv = "\uFEFF";
        var cont = 0;
        var result;
        var columns = data.getColumns();
        if (cont === 1) {
            result = data.props.selection || [];
        } else {
            result = [].concat(_toConsumableArray(data.props.frozenValue || []), _toConsumableArray(data.processedData() || []));
        }
        //headers
        columns.forEach(function (column, i) {
            var _column$props = column.props,
                field = _column$props.field,
                header = _column$props.header,
                exportable = _column$props.exportable;
            if (exportable && field) {
                csv +=(header || field);
                headers = [...headers, header || field];
                var Json = JSON.stringify(headers);
                console.log(Json);

                if (i < columns.length - 1) {
                    csv += data.props.csvSeparator;
                    //csv += ' ';
                }
            }
            console.log(headers);
        });

        //body
        result.forEach(function (record) {
            csv += '\n';
            columns.forEach(function (column, i) {
                var _column$props2 = column.props,
                    columnField = _column$props2.field,
                    header = _column$props2.header,
                    exportField = _column$props2.exportField,
                    exportable = _column$props2.exportable;
                var field = exportField || columnField;

                console.log(record);
                console.log(field);
                if (exportable && field) {
                    var cellData = ObjectUtils.resolveFieldData(record, field);
                    var value2 = ObjectUtils.getRefElement(record, field);
                    console.log(value2);
                    console.log(cellData);

                    if (cellData != null) {
                        cellData = data.props.exportFunction ? data.props.exportFunction({
                            data: cellData,
                            field: field,
                            rowData: record,
                            column: column
                        }) : String(cellData).replace(/"/g, '""');
                    } else cellData = '';

                    csv +=cellData;

                    bodys = [...bodys, cellData];
                    //console.log(bodys);
                    //headers[columns.length].concat(bodys);
                    //console.log(headers);
                    if (i < columns.length - 1) {

                        csv += data.props.csvSeparator;
                        //csv += ' ';
                    }
                }
                console.log(bodys);
            });
        });
        //DomHandler.exportCSV(csv, "reportes");
        //const fs = require("fs");
        //csv = fs.readFileSync("reportes.csv")

        const array = csv.toString().split('\n');
        console.log(array);

        /* Store the converted result into an array */
        const csvToJsonResult = [];

        /* Store the CSV column headers into seprate variable */
        const headerss = array[0].split(",")
        console.log(headerss);

        /* Iterate over the remaning data rows */
        for (let i = 1; i < array.length - 1; i++) {
        /* Empty object to store result in key value pair */
        const jsonObject = {}
        /* Store the current array element */
        const currentArrayString = array[i]
        let string = ''

        let quoteFlag = 0
        for (let character of currentArrayString) {
            if (character === '"' && quoteFlag === 0) {
                quoteFlag = 1
            }
            else if (character === '"' && quoteFlag == 1) quoteFlag = 0
            if (character === ',' && quoteFlag === 0) character = '|'
            if (character !== '"') string += character
        }

        let jsonProperties = string.split("|")

        for (let j in headerss) {
            if (jsonProperties[j].includes(",")) {
            jsonObject[headerss[j]] = jsonProperties[j]
                .split(",").map(item => item.trim())
            }
            else jsonObject[headerss[j]] = jsonProperties[j]
        }
        /* Push the genearted JSON object to resultant array */
        csvToJsonResult.push(jsonObject)
        }
        /* Convert the final array to JSON */
        const json = JSON.stringify(csvToJsonResult);
        const jsonParse = JSON.parse(json);
        console.log(json)
        console.log(jsonParse)
        excel = headerss.concat(bodys);
        console.log(excel);
        console.log(csv);
        console.log(result);
        console.log(columns);
        import('xlsx').then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet(jsonParse);
            //const worksheet = xlsx.current.json_to_sheet;
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            saveAsExcelFile(excelBuffer, 'products');
        });
    }
    const exportExcell = (selectionOnly) => {
        tableRef.current.exportExcel({ selectionOnly });
        //tableRef.current.exportCSV({ selectionOnly });
    }
    const exportCSV = (selectionOnly) => {
        let result = tableRef.current.exportCSV({ selectionOnly });
        console.log(result);
        //tableRef.current.exportCSV({ selectionOnly });
    }


    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then(module => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });

                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    }
    const header = (
        <div className="flex align-items-center export-buttons">
            <Button type="button" icon="pi pi-file" onClick={() => exportCSV(false)} className="mr-2" data-pr-tooltip="CSV" />
            <Button type="button" icon="pi pi-file-excel" onClick={() => exportExcel(false)} className="mr-p-button-success mr-2" data-pr-tooltip="XLS" />
            {/* <Button type="button" icon="pi pi-file-pdf" onClick={exportPdf} className="p-button-warning mr-2" data-pr-tooltip="PDF" /> */}
            {/* <Button type="button" icon="pi pi-filter" onClick={() => exportCSV(true)} className="p-button-info ml-auto" data-pr-tooltip="Selection Only" /> */}
        </div>
    );
    const onSelectionChange = (e) => {
        setSelectedProducts(e.value);
    }

    return (
        <React.Fragment>
            <div className="card">
                <h5>Export</h5>

                <Tooltip target=".export-buttons>button" position="bottom" />

                <DataTable id="table-excel" ref={tableRef} value={tableBiopsia} header={header} dataKey="id" responsiveLayout="scroll"
                    selectionMode="multiple" selection={selectedProducts} onSelectionChange={onSelectionChange}>
                    {
                        cols.map((col, index) => <Column key={index} field={col.name} header={col.label} />)
                    }
                </DataTable>
            </div>

        </React.Fragment>
    )
}
export default ReportesTable;

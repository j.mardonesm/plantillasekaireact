import React, { useState, useEffect, useRef } from "react";
import { classNames } from "primereact/utils";
import { useForm, Controller } from "react-hook-form";
import { PickList } from 'primereact/picklist';
import { Button } from "primereact/button";
import { Sidebar } from "primereact/sidebar";
import ReportesTable from "./ReportesTable";

const ReportesListBusqueda = () => {
    const [source, setSource] = useState([]);
    const [target, setTarget] = useState([]);
    const [visibleFullScreen, setVisibleFullScreen] = useState(false);
    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();
    const Variables = [
        { "id": 1, "name": "nrobiopsia", "label": "Número Biopsia" },
        { "id": 2, "name": "medicotratante", "label" : "Médico Tratante" },
        { "id": 3, "name": "auge", "label": "Auge" },
        { "id": 4, "name": "prevision", "label": "Previsión" },
        { "id": 5, "name": "servicio", "label": "Servicio de Origen" },
        { "id": 6, "name": "formatomuestraid", "label":"Presentación de la Muestra" },
        { "id": 7, "name": "cantidadmuestra", "label": "Cantidad De la Muestra"},
        { "id": 8, "name": "fechatomamuestra", "label":"Fecha Toma de la Muestra" },
        { "id": 9, "name": "fecharecepcionmuestra", "label":"Fecha Recepción de la muestra" },
        { "id": 10, "name": "muestraenviada", "label": "Muestra Enviada"},
        { "id": 11, "name": "antecedentes", "label": "Antecedentes y/o probable Diagnostico"},
        { "id": 12, "name": "organo1", "label":"Organo Primario" },
        { "id": 13, "name": "organo2", "label": "Organo Secundario"},
        { "id": 14, "name": "patologo", "label": "Patólogo Responsable"},
        { "id": 15, "name": "tecnologo", "label": "Tecnólogo Responsable"},
        { "id": 16, "name": "biopsia", "label": "Biopsia"},
        { "id": 17, "name": "biopsiarapida", "label":"Biopsia Rápida" },
        { "id": 18, "name": "citologico", "label":"Citología" },
        { "id": 19, "name": "necropsia", "label": "Necropsia"},
        { "id": 20, "name": "placas", "label": "Placas BR"},
        { "id": 21, "name": "tipoinformeid", "label": "Tipo de Informe"},
        { "id": 22, "name": "informeprocesado", "label": "Estado de Informe"},
        { "id": 23, "name": "dias", "label":"Dias desde Recepción" },
        { "id": 24, "name": "fechainforme", "label": "Fecha del Informe"},
        { "id": 25, "name": "fechaentregainforme", "label": "Fecha Entrega del Informe"},
        { "id": 26, "name": "diagnostico", "label":"Diagnóstico" },
        { "id": 27, "name": "Macroscopía y/o Microscopía", "label": "Macroscopía y/o Microscopía" },


    ]
    useEffect(() => {
        setSource(Variables);
    }, []);
    const onChange = (event) => {
        setSource(event.source);
        setTarget(event.target);
    }
    const itemTemplate = (item) => {
        return (
            <div className="product-item">
                <div className="product-list-detail">
                    <h5 className="mb-2">{item.label}</h5>
                </div>
            </div>
        );
    }
    const submitBusqueda = () => {
        console.log(target);
        setVisibleFullScreen(true);
    }
    return (
        <React.Fragment>
            <div>
                <div className="picklist-demo">
                    <PickList source={source} target={target} itemTemplate={itemTemplate} sourceHeader="Campos Disponibles" targetHeader="Campos Seleccionados"
                        sourceStyle={{ height: '342px' }} targetStyle={{ height: '342px' }} onChange={onChange}
                        filterBy="label" sourceFilterPlaceholder="Search by label" targetFilterPlaceholder="Search by label" />

                </div>
                <Button type="button" id="BusquedaBiopsia" className="p-button-primary" icon="pi pi-search" onClick={() => submitBusqueda()} />
            </div>
            <Sidebar visible={visibleFullScreen} onHide={() => setVisibleFullScreen(false)} baseZIndex={1000} fullScreen>
                <ReportesTable Campos={target}/>
            </Sidebar>
        </React.Fragment>

    );
};
export default ReportesListBusqueda;

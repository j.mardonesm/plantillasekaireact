import axios from "axios";


export async function searchBiopsiaByid(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/id/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}


export async function searchNeoplasia(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-diagnostico/api/diagnostico/neoplasia/true`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}

export async function searchBiopsias() {

    try {
      const response = await fetch("http://10.69.206.32:8080/api-apa-biopsia/api/biopsia", {
        method: 'GET',
        headers: {}
      });

      if (response.ok) {
        const result = await response.json();
        console.log(result);
      }
    } catch (err) {
      console.error(err);
    }}

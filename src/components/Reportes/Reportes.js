import React, { useState, useEffect, useRef } from "react";
import { classNames } from "primereact/utils";
import { useForm, Controller } from "react-hook-form";
import { PickList } from 'primereact/picklist';
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dropdown } from "primereact/dropdown";
import { InputNumber } from "primereact/inputnumber";
import { Button } from "primereact/button";
import { ProgressBar } from "primereact/progressbar";
import { Calendar } from "primereact/calendar";
import { MultiSelect } from "primereact/multiselect";
import { Slider } from "primereact/slider";
import { Sidebar } from "primereact/sidebar";
import { InputText } from "primereact/inputtext";
import { Toolbar } from "primereact/toolbar";
import { Toast } from "primereact/toast";
import { FileUpload } from "primereact/fileupload";
import { searchBiopsia, searchBiopsias } from "./ReportesService";
import Axios from "axios";
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import { TabPanel, TabView } from "primereact/tabview";
import { ConfirmDialog } from "primereact/confirmdialog";
import ReportesListBusqueda from "./ReportesListBusqueda";

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const Reporte = () => {
    const [customers1, setCustomers1] = useState(null);
    const [biopsias, setBiopsias] = useState();

    useEffect(() => {
        Axios.get("http://10.69.206.32:8080/api-apa-biopsia/api/biopsia")
            .then(response => {
                setBiopsias(response.data)
            }
            )
    })

    const Parametros = [
        {
            id: 1,
            name: "Patólogo Responsable",
        },
        {
            id: 2,
            name: "Tecnólogo Responsable",
        },
        {
            id: 3,
            name: "Órgano Primario",
        },
        {
            id: 4,
            name: "Servicio Clínico",
        },
    ];
    const ValoresMuestra = [
        {
            id: 1,
            name: "Tipo Órgano",
        },
        {
            id: 2,
            name: "Diagnóstico",
        },
        {
            id: 3,
            name: "Macroscopia/Microscopia",
        },
        {
            id: 4,
            name: "Fecha Recepción",
        },
    ];
    let emptyProduct = {
        id: null,
        name: "",
        image: null,
        description: "",
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: "INSTOCK",
    };
    const [product, setProduct] = useState(emptyProduct);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [card1, setCard1] = useState();
    const [card2, setCard2] = useState();
    const [card3, setCard3] = useState();
    const [card4, setCard4] = useState();
    const [visibleFullScreen, setVisibleFullScreen] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [productDialog, setProductDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [loading3, setLoading3] = useState(false);
    const [loading1, setLoading1] = useState(false);
    const [loading2, setLoading2] = useState(false);
    const dt = useRef(null);
    const toast = useRef(null);
    const [posts, setPosts] = useState([]);
    const [activeIndex, setActiveIndex] = useState(0);

    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();

    const CriterioBiopsia = [
        {
            "id": 1,
            "busqueda": "Médico Tratante"
        },
        {
            "id": 2,
            "busqueda": "Servicio Origen"
        },
        {
            "id": 3,
            "busqueda": "Fecha Recepción Muestra"
        },
        {
            "id": 4,
            "busqueda": "Fecha Toma Muestra"
        },

    ];
    const submitBusquedaBiopsia = (data) => {
        console.log(data);
    };
    const submitBusquedaNeoplasia = (data) => {
        console.log(data);
    };

    const [selectedProducts, setSelectedProducts] = useState(null);

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Reporte</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const openNew = () => {
        setProduct(emptyProduct);
        setSubmitted(false);
        setProductDialog(true);
    };

    const exportCSV = () => {
        dt.current.exportCSV();
    };

    const exportExcel = () => {
        import("xlsx").then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(biopsias);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
            const excelBuffer = xlsx.write(workbook, { bookType: "xlsx", type: "array" });
            saveAsExcelFile(excelBuffer, "InfoBiopsias");
        });
    };




    const saveAsExcelFile = (buffer, fileName) => {
        import("file-saver").then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
                let EXCEL_EXTENSION = ".xlsx";
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE,
                });
                module.default.saveAs(data, fileName + "_export_" + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };


    const confirmDeleteSelected = () => {
        setDeleteProductsDialog(true);
    };


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedProducts || !selectedProducts.length} />
                </div>
            </React.Fragment>
        );
    };

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                    <Button label="CSV" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
                    <Button label="EXCEL" icon="pi pi-upload" className="p-button-help" onClick={exportExcel} />
                    <Button label="PDF" icon="pi pi-upload" className="p-button-help" onClick={downloadPdf} />

                </div>
            </React.Fragment>
        );
    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };


    const columns = [
        { title: "nrobiopsia", field: "nrobiopsia", },
        { title: "auge", field: "auge", },
        { title: "personaid", field: "personaid", },
        { title: "medicotratante", field: 'medicotratante', },
        { title: "fechatomamuestra", field: 'fechatomamuestra', },
        { title: "fecharecepcionmuestra", field: 'fecharecepcionmuestra', },
        { title: "servicio", field: 'servicio', },
        { title: "organo1", field: 'organo1', },
        { title: "organo2", field: 'organo2', },
        { title: "patologo", field: 'patologo', },
        { title: "tecnologo", field: 'tecnologo', },
        { title: "dias", field: 'dias', },
        { title: "fechainforme", field: 'fechainforme', },
        { title: "fechaentregainforme", field: 'fechaentregainforme', },
        { title: "biopsia", field: 'biopsia', },
        { title: "biopsiarapida", field: 'biopsiarapida', },
        { title: "citologico", field: 'citologico', },
        { title: "necropsia", field: 'necropsia', },
        { title: "placas", field: 'placas', },
        { title: "placas", field: 'placas', },
        { title: "muestraenviada", field: 'muestraenviada', },
        { title: "antecedentes", field: 'antecedentes', },
        { title: "tipoinformeid", field: 'tipoinformeid', },
        { title: "formatomuestraid", field: 'formatomuestraid', },
        { title: "cantidadmuestra", field: 'cantidadmuestra', },
        { title: "prevision", field: 'prevision', },
        { title: "informeprocesado", field: 'informeprocesado', },


    ]

    const downloadPdf = () => {
        const doc = new jsPDF({});
        doc.text("Student Details", 20, 10)
        doc.autoTable({
            theme: "grid",
            columns: columns.map(col => ({ ...col, dataKey: col.field })),
            body: biopsias
        })
        doc.save('table.pdf')
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////// Termino Componentes   ///////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    return (

        <div>
            <div className="p-grid">
                <div className="col-12">
                    <div className="card">
                        <Toast ref={toast} position="bottom-center" />
                        <h5>Reportería</h5>
                        <hr />
                        <h2>Filtros</h2>
                        <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                            <TabPanel header="Busqueda por Biopsia">
                                <Toast ref={toast} />
                                <ConfirmDialog />
                                <div className="field col-12 md:col-5">
                                    <form onSubmit={handleSubmit(submitBusquedaBiopsia)}>
                                        <br />
                                        <br />
                                        <div className="p-inputgroup">
                                            <span className="p-float-label">
                                                <Controller name="BusquedaBiopsia" control={control} rules={{ required: "Debe Seleccionar un criterio de búsqueda" }} render={({ field, fieldState }) =>
                                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} options={CriterioBiopsia} optionLabel="busqueda" />} />
                                                <label htmlFor="BusquedaBiopsia" className={classNames({ 'p-error': errors.name })}>Criterio de Búsqueda</label>
                                                {/* <InputText id="PacienteRut"  name="pacRut" type="text" maxLength={10}
                                                            {...register("pacRut", { required: true, maxLength: 10 })} /> */}
                                            </span>
                                            <Button id="BusquedaBiopsia" className="p-button-primary" icon="pi pi-search" loading={loading3} />
                                        </div>
                                        {getFormErrorMessage('pacRut')}

                                        {/* <span className="p-error">{errors?.pacRut?.type === "required" && <span className="p-error">El Rut es requerido</span>}</span> */}
                                    </form>
                                </div>
                            </TabPanel>
                            <TabPanel header="Neoplasia">
                                <div className="field col-12 md:col-8">
                                    <form onSubmit={handleSubmit(submitBusquedaNeoplasia)}>


                                        <h2>Seleccione los campos que desea exportar</h2>
                                        <br />
                                        <br />
                                        <ReportesListBusqueda/>
                                    </form>
                                </div>
                            </TabPanel>
                        </TabView>
                    </div>
                </div>
            </div>
        </div>

    );
};
const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};
export default React.memo(Reporte, comparisonFn);

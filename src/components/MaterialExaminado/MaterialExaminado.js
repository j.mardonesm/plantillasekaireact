import { FaSave } from "@react-icons/all-files/fa/FaSave";
import { Button } from "primereact/button";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import React, { useState, useEffect, useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { listMateriales } from "./MaterialExaminadoService";


const MaterialExaminado = () => {
    const [materiales, setMateriales] = useState([]);
    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();

    useEffect(() => {
        search();
    }, [])
    const search = async () => {
        let result = await listMateriales();
        console.log(result);
        setMateriales(result);

    };
    const submitMats = async (data) => {
        console.log(data)
    };
    return (
        <React.Fragment>
            <form onSubmit={handleSubmit(submitMats)}>
                <div className="p-fluid formgrid grid">
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="card-header">
                                <h3>Fecha proceso</h3>
                            </div>
                            <div className="card-body">
                                <Controller name="FechaProceso" control={control} render={({ field }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="card-header">
                                <h3>Fecha Entrega</h3>
                            </div>
                            <div className="card-body">
                                <Controller name="FechaEntrega" control={control} render={({ field }) => (
                                    <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} dateFormat="dd/mm/yy" mask="99/99/9999" showIcon showButtonBar locale="es" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Hematoxilina Eosina</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantHematoxilina">Cantidad</label>
                                <Controller name="CantHematoxilina" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Moldes</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantMoldes">Cantidad</label>
                                <Controller name="CantMoldes" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Citología</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantCitologias">Cantidad</label>
                                <Controller name="CantCitologias" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Recortes</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantRecortes">Cantidad</label>
                                <Controller name="CantRecortes" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Placas BR</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantPlacasBR">Cantidad</label>
                                <Controller name="CantPlacasBR" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Placas Totales</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantPlacasTotales">Cantidad</label>
                                <Controller name="CantPlacasTotales" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="card-header">
                                <h3>Histoquímica</h3>
                            </div>
                            <div className="card-body">
                                <label htmlFor="CantHistoquimica">Cantidad</label>
                                <Controller name="CantHistoquimica" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />
                                <label htmlFor="Tincion1">Técnica 1</label>
                                <Controller name="Tincion1" control={control} render={({ field }) => (
                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={materiales} optionLabel="descripcion" />
                                )} />
                                <label htmlFor="Tincion2">Técnica 2</label>
                                <Controller name="Tincion2" control={control} render={({ field }) => (
                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={materiales} optionLabel="descripcion" />
                                )} />
                                <label htmlFor="Tincion3">Técnica 3</label>
                                <Controller name="Tincion3" control={control} render={({ field }) => (
                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={materiales} optionLabel="descripcion" />
                                )} />
                                <label htmlFor="Tincion4">Técnica 4</label>
                                <Controller name="Tincion4" control={control} render={({ field }) => (
                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={materiales} optionLabel="descripcion" />
                                )} />
                                <label htmlFor="Tincion5">Técnica 5</label>
                                <Controller name="Tincion5" control={control} render={({ field }) => (
                                    <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={materiales} optionLabel="descripcion" />
                                )} />
                                <label htmlFor="DescTinciones">Descripción</label>
                                <Controller name="DescTinciones" control={control} render={({ field }) => (
                                    <InputTextarea id={field.name} {...field} placeholder="" autoResize rows="6" cols="30" />
                                )} />

                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="card-header">
                                <h3>Inmunohistoquímica</h3>
                            </div>
                            <div className="card-body">

                                <label htmlFor="CantInmuno">Cantidad</label>
                                <Controller name="CantInmuno" defaultValue={0} control={control} render={({ field }) => (
                                    <InputText type="number" value={field.value} id={field.name} {...field} mode="decimal" min="0" />
                                )} />
                                <label htmlFor="Descinmuno">Descripción</label>
                                <Controller name="Descinmuno" control={control} render={({ field }) => (
                                    <InputTextarea id={field.name} {...field} placeholder="" autoResize rows="6" cols="30" />
                                )} />
                            </div>
                        </div>
                    </div>

                </div>
                <div className="flex flex-row-reverse flex-wrap card-container blue-container">
                    <div className="px-1">
                        <Button type="submit" icon={<FaSave />} className=" p-button-raised p-button-warning" />
                    </div>
                </div>
            </form>
        </React.Fragment>
    );
}
export default MaterialExaminado;

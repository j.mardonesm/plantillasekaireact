import axios from 'axios';



export async function listMateriales(){
    let url = process.env.REACT_APP_API_URL + 'api-apa-tincion/api/';
    try {
        const response = await axios.get(url + 'tincion')
        return response.data;
    } catch (error) {
        console.error(error);
        return null;
    }
}

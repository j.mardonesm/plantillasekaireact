import React, { useState, useEffect, useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { locale, addLocale } from "primereact/api";
import { InputText } from "primereact/inputtext";
import { classNames, ConnectedOverlayScrollHandler } from "primereact/utils";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import ResumenEstadisticas from "./ResumenEstadisticas";
import DiarioEstadisticas from "./DiarioEstadisticas";
import ServicioEstadisticas from "./ServicioEstadisticas";
import { TabPanel, TabView } from "primereact/tabview";
import { Toast } from "primereact/toast";
import { ConfirmDialog } from "primereact/confirmdialog";
import { BiopsiaByFecha, CodFonasa, listServicios } from "./EstadisticasService";
import Swal from "sweetalert2";

addLocale("es", {
    firstDayOfWeek: 1,
    dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
    dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
    today: "Hoy",
    clear: "Limpiar",
});
locale("es");
const Estadisticas = () => {
    const ServiciosdeOrigen = [
        {
            id: 1,
            name: "Inst. Chileno Japones",
        },
        {
            id: 2,
            name: "Dermatología",
        },
        {
            id: 3,
            name: "Endocrinología",
        },
        {
            id: 4,
            name: "Hematología",
        },
    ];




    const [loading2, setLoading2] = useState(false);
    const [verEstadisticas, setVerEstadisticas] = useState(false);
    const [verDiarios, setVerDiarios] = useState(false);
    const [verServicio, setVerServicio] = useState(false);
    const [activeIndex, setActiveIndex] = useState(0);
    const [disabledTab, setDisabledTab] = useState(false);
    const isEven = (value)=>value % 2 === 0;
    const isOdd = (value) => value => !isEven(value);


    const [sumBiopsias, setSumBiopsias] = useState(0);
    const [sumBiopsiaRapida, setSumBiopsiaRapida] = useState(0);
    const [sumCitologica, setSumCitologica] = useState(0);
    const [sumNecropsia, setSumNecropsia] = useState(0);

    const [sum1001, setsum1001] = useState(0);
    const [sum1002, setsum1002] = useState(0);
    const [sum1003, setsum1003] = useState(0);
    const [sum1004, setsum1004] = useState(0);
    const [sum1005, setsum1005] = useState(0);
    const [sum1006, setsum1006] = useState(0);
    const [sum1007, setsum1007] = useState(0);
    const [sum1008, setsum1008] = useState(0);
    const [sum1009, setsum1009] = useState(0);
    const [sum1010, setsum1010] = useState(0);
    const [servicios, setServicios] = useState([]);

    const toast = useRef(null);
    let fechaHoy = new Date();
    const today = fechaHoy;
    const {
        control,
        formState: { errors },
        handleSubmit,
        reset,
    } = useForm();

    


    useEffect(() => {

        search();

    },[]);

    const search = async() => 
    {
        let result = await listServicios();
        setServicios(result);
        console.log(result);

    }

    async function renderizado(result) {
        let sum1001 = 0;
        let sum1002 = 0;
        let sum1003 = 0;
        let sum1004 = 0;
        let sum1005 = 0;
        let sum1006 = 0;
        let sum1007 = 0;
        let sum1008 = 0;
        let sum1009 = 0;
        let sum1010 = 0;
        let sumBiopsias = 0;
        let sumBiopsiaRapida = 0;
        let sumCitologica = 0;
        let sumNecropsia = 0;
        var sumCollection = [];

        result.map(async (element) => {
            sumCollection = [...sumCollection, element.id];

            sumBiopsias = sumBiopsias + element.biopsia;
            sumCitologica = sumCitologica + element.citologico;
            sumBiopsiaRapida = sumBiopsiaRapida + element.biopsiarapida;
            sumNecropsia = sumNecropsia + element.necropsia;
        });


        await Promise.all(
            sumCollection.map(async (e) => {
                let sum = await CodFonasa(e);
                //console.log(sum.codigo);

                for (var el in sum) {
                    switch (sum[el].codigo) {
                        case 801001:
                            sum1001 = sum1001 + sum[el].cantidad;
                            break;
                        case 801002:
                            sum1002 = sum1002 + sum[el].cantidad;
                            break;
                        case 801003:
                            sum1003 = sum1003 + sum[el].cantidad;
                            break;
                        case 801004:
                            sum1004 = sum1004 + sum[el].cantidad;
                            break;
                        case 801005:
                            sum1005 = sum1005 + sum[el].cantidad;
                            break;
                        case 801006:
                            sum1006 = sum1006 + sum[el].cantidad;
                            break;
                        case 801007:
                            sum1007 = sum1007 + sum[el].cantidad;
                            break;
                        case 801008:
                            sum1008 = sum1008 + sum[el].cantidad;
                            break;
                        case 801009:
                            sum1009 = sum1009 + sum[el].cantidad;
                            break;
                        case 801010:
                            sum1010 = sum1010 + sum[el].cantidad;
                            break;
                        default:
                            break;
                         }}}));
    

        console.log(collection);

        //   setTimeout(() => {
        var collection = [
            {
                sumBiopsias: sumBiopsias,
                sumCitologica: sumCitologica,
                sumBiopsiaRapida: sumBiopsiaRapida,
                sumNecropsia: sumNecropsia,
                sum1001: sum1001, 
                sum1002: sum1002,
                sum1003: sum1003,
                sum1004: sum1004,
                sum1005: sum1005,
                sum1006: sum1006,
                sum1007: sum1007,
                sum1008: sum1008,
                sum1009: sum1009,
                sum1010: sum1010,
            },
        ];
        //console.log(collection);
        return collection;
        // }, );
    }
   
    const successAlert = () => {
        Swal.fire({
            title: 'Alerta!',
            text: 'Busqueda sin resultados',
            icon: 'warning',
        });
    };


    const submitEstadisticas = async (data) => {
        //#region Resumen Biopsias
        console.log(data);

        let desde;
        let hasta;

        desde = data.fechaDesde;
        hasta = data.fechaHasta;
        let desdeFormateada = desde;
        let hastaFormateada = hasta;
        desde = new Intl.DateTimeFormat("fr-CA").format(desdeFormateada);
        hasta = new Intl.DateTimeFormat("fr-CA").format(hastaFormateada);

        let result = await BiopsiaByFecha(desde, hasta);

        var arreglo = [];

        let sumas;
        if (data.Servicio=== undefined) {
            sumas= await renderizado(result);
        }else{
             result.map (async (element) => {
            if (element.servicio === data.Servicio.id)
            {
                arreglo = [...arreglo, element];
                
            };
        });
        if(arreglo.length>0)
        {
                sumas= await renderizado(arreglo);

        }else{
            return successAlert();

        }
        }
       

       

        if (arreglo.length === 0 )
        {
           
        }else {
        }


        console.log(arreglo);
        //console.log(sumas);
        //console.log(sumas);
        // console.log(biop);

       // console.log(sumas[0].sum1001);


        setsum1001(sumas[0].sum1001);
        setsum1002(sumas[0].sum1001);
        setsum1002(sumas[0].sum1002);
        setsum1003(sumas[0].sum1003);
        setsum1004(sumas[0].sum1004);
        setsum1005(sumas[0].sum1005);
        setsum1006(sumas[0].sum1006);
        setsum1007(sumas[0].sum1007);
        setsum1008(sumas[0].sum1008);
        setsum1009(sumas[0].sum1009);
        setsum1010(sumas[0].sum1010);


        // console.log(sumBiopsias);
        // console.log(sumCitologica);
        // console.log(sumBiopsiaRapida);
        // console.log(sumNecropsia);


        setSumBiopsiaRapida(sumas[0].sumBiopsiaRapida);
        setSumCitologica(sumas[0].sumCitologica);
        setSumBiopsias(sumas[0].sumBiopsias);
        setSumNecropsia(sumas[0].sumNecropsia);
        
       
        
        //#endregion
        // console.log(data);
        setVerEstadisticas(!verEstadisticas);
        setDisabledTab(!disabledTab);
        reset();
    };


    const handleTabChange = async (name) => {
        setDisabledTab(!disabledTab);

        if (name === "estadistica") {
            setVerEstadisticas(!verEstadisticas);
        } else if (name === "diario") {
            setVerDiarios(!verDiarios);
        } else if (name === "servicio") {
            setVerServicio(!verServicio);
        }
    };


    const submitDiario = async (data) => {
        setVerDiarios(!verDiarios);
        setDisabledTab(!disabledTab);
        reset();
    };

    const submitServicio = async (data) => {
        setVerServicio(!verServicio);
        setDisabledTab(!disabledTab);
        reset();
    };

    const getFormErrorMessage = (name) => {
        return errors[name] && <small className="p-error">{errors[name].message}</small>;
    };

    return (
        <React.Fragment>
            <div className="p-grid">
                <div className="col-15 md:col-12">
                    <div className="card">
                        <h2>Estadísticas</h2>
                        <hr />
                        <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>
                            <TabPanel header="General" disabled={disabledTab}>
                                <Toast ref={toast} />
                                <ConfirmDialog />
                                <form onSubmit={handleSubmit(submitEstadisticas)}>
                                    <h5>Filtros de Busqueda</h5>
                                    <div className="p-fluid grid flex flex-wrap align-items-center justify-content-center card-container">
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="fechaDesde">Desde</label>
                                                <Controller
                                                    name="fechaDesde"
                                                    control={control}
                                                    rules={{ required: "Debe existir una fecha de inicio" }}
                                                    render={({ field, fieldState }) => (
                                                        <Calendar
                                                            id={field.name}
                                                            value={field.value}
                                                            onChange={(e) => field.onChange(e.value)}
                                                            dateFormat="dd/mm/yy"
                                                            mask="99/99/9999"
                                                            showIcon
                                                            showButtonBar
                                                            locale="es"
                                                            disabledDays={[0, 6]}
                                                            readOnlyInput
                                                            className={classNames({ "p-invalid": fieldState.invalid })}
                                                        />
                                                    )}
                                                />
                                            </div>
                                            {getFormErrorMessage("fechaDesde")}
                                        </div>
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="fechaHasta">Hasta</label>
                                                <Controller
                                                    name="fechaHasta"
                                                    control={control}
                                                    rules={{ required: "Debe existir una fecha final" }}
                                                    render={({ field, fieldState }) => (
                                                        <Calendar
                                                            id={field.name}
                                                            value={field.value}
                                                            onChange={(e) => field.onChange(e.value)}
                                                            maxDate={today}
                                                            dateFormat="dd/mm/yy"
                                                            mask="99/99/9999"
                                                            showIcon
                                                            showButtonBar
                                                            locale="es"
                                                            disabledDays={[0, 6]}
                                                            readOnlyInput
                                                            className={classNames({ "p-invalid": fieldState.invalid })}
                                                        />)}/>
                                            </div>
                                            {getFormErrorMessage("fechaHasta")}
                                        </div>
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="Servicio">Servicio</label>
                                                <Controller name="Servicio" control={control} render={({ field }) => <Dropdown id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} placeholder="Seleccione" options={servicios} optionLabel="descripcion" />} />
                                            </div>
                                        </div>
                                        <div className="pt-5 field col-12 md:col-1">
                                            <div>
                                                <Button type="submit" className="mr-2 mb-0" icon="pi pi-search" loading={loading2} disabled={verEstadisticas} />
                                            </div>
                                        </div>
                                        <div className="pt-5 field col-12 md:col-1" hidden={!verEstadisticas}>
                                            <div>
                                                <Button type="button" className="p-button-danger mr-2 mb-0" icon="pi pi-times" loading={loading2} onClick={() => handleTabChange("estadistica")} disabled={!verEstadisticas} />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </TabPanel>
                            <TabPanel header="Desglose Diario" disabled={disabledTab}>
                                <form onSubmit={handleSubmit(submitDiario)}>
                                    <h5>Filtros de Busqueda</h5>
                                    <div className="p-fluid grid flex flex-wrap align-items-center justify-content-center card-container">
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="MesBusqueda">Mes y Año</label>
                                                <Controller
                                                    name="MesBusqueda"
                                                    control={control}
                                                    rules={{ required: "Debe seleccionar una opción para la busqueda" }}
                                                    render={({ field, fieldState }) => (
                                                        <Calendar id={field.name} value={field.value} onChange={(e) => field.onChange(e.value)} view="month" dateFormat="mm/yy" showIcon locale="es" readOnlyInput className={classNames({ "p-invalid": fieldState.invalid })} />
                                                    )}
                                                />
                                            </div>
                                            {getFormErrorMessage("MesBusqueda")}
                                        </div>
                                        <div className="px-2 pt-5 field col-12 md:col-1">
                                            <div>
                                                <Button type="submit" className="mr-2 mb-0" icon="pi pi-search" loading={loading2} />
                                            </div>
                                        </div>
                                        <div className="pt-5 field col-12 md:col-1" hidden={!verDiarios}>
                                            <div>
                                                <Button type="button" className="p-button-danger mr-2 mb-0" icon="pi pi-times" loading={loading2} onClick={() => handleTabChange("diario")} disabled={!verDiarios} />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </TabPanel>
                            <TabPanel header="Desglose Servicio" disabled={disabledTab}>
                                <form onSubmit={handleSubmit(submitServicio)}>
                                    <h5>Filtros de Busqueda</h5>
                                    <div className="p-fluid grid flex flex-wrap align-items-center justify-content-center card-container">
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="fechaDesde">Desde</label>
                                                <Controller
                                                    name="fechaDesde"
                                                    control={control}
                                                    rules={{ required: "Debe existir una fecha de inicio" }}
                                                    render={({ field, fieldState }) => (
                                                        <Calendar
                                                            id={field.name}
                                                            value={field.value}
                                                            onChange={(e) => field.onChange(e.value)}
                                                            dateFormat="dd/mm/yy"
                                                            mask="99/99/9999"
                                                            showIcon
                                                            showButtonBar
                                                            locale="es"
                                                            disabledDays={[0, 6]}
                                                            readOnlyInput
                                                            className={classNames({ "p-invalid": fieldState.invalid })}
                                                        />
                                                    )}
                                                />
                                            </div>
                                            {getFormErrorMessage("fechaDesde")}
                                        </div>
                                        <div className="px-6 field col-15 md:col-3">
                                            <div>
                                                <label htmlFor="fechaHasta">Hasta</label>
                                                <Controller
                                                    name="fechaHasta"
                                                    control={control}
                                                    rules={{ required: "Debe existir una fecha final" }}
                                                    render={({ field, fieldState }) => (
                                                        <Calendar
                                                            id={field.name}
                                                            value={field.value}
                                                            onChange={(e) => field.onChange(e.value)}
                                                            maxDate={today}
                                                            dateFormat="dd/mm/yy"
                                                            mask="99/99/9999"
                                                            showIcon
                                                            showButtonBar
                                                            locale="es"
                                                            disabledDays={[0, 6]}
                                                            readOnlyInput
                                                            className={classNames({ "p-invalid": fieldState.invalid })}
                                                        />
                                                    )}
                                                />
                                            </div>
                                            {getFormErrorMessage("fechaHasta")}
                                        </div>
                                        <div className="px-2 pt-5 field col-12 md:col-1">
                                            <div>
                                                <Button type="submit" className="mr-2 mb-0" icon="pi pi-search" loading={loading2} />
                                            </div>
                                        </div>
                                        <div className="pt-5 field col-12 md:col-1" hidden={!verServicio}>
                                            <div>
                                                <Button type="button" className="p-button-danger mr-2 mb-0" icon="pi pi-times" loading={loading2} onClick={() => handleTabChange("servicio")} disabled={!verServicio} />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </TabPanel>
                        </TabView>
                    </div>
                </div>
            </div>

            <div className={verEstadisticas ? "show-element" : null}>
                {verEstadisticas && (
                    <ResumenEstadisticas
                        resumenBiopRapida={sumBiopsiaRapida}
                        resumenBiop={sumBiopsias}
                        resumenCitolog={sumCitologica}
                        resumenNecropsia={sumNecropsia}
                        sum1001={sum1001}
                        sum1002={sum1002}
                        sum1003={sum1003}
                        sum1004={sum1004}
                        sum1005={sum1005}
                        sum1006={sum1006}
                        sum1007={sum1007}
                        sum1008={sum1008}
                        sum1009={sum1009}
                        sum1010={sum1010}
                    />
                )}
            </div>
            <div className={verDiarios ? "show-element" : null}>{verDiarios && <DiarioEstadisticas />}</div>
            <div className={verServicio ? "show-element" : null}>{verServicio && <ServicioEstadisticas />}</div>
        </React.Fragment>
    );
};
export default React.memo(Estadisticas);




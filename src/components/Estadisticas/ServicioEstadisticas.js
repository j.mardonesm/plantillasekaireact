import React, { useState, useEffect, useRef } from "react";
import { Button } from "primereact/button";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Tooltip } from "primereact/tooltip";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ServicioEstadisticas= () => {
    const [products, setProducts] = useState([]);
    const [selectedProducts, setSelectedProducts] = useState([]);
    const dt = useRef(null);
    const biopsias =[{
        id: 1,
        servicio:'Cirugía adulto',
        biopsias: 4,
        BRapida: 10,
        citologica:15,
        necropsia:4
    },
    {
        id: 2,
        servicio:'Ins. Chileno Japones',
        biopsias: 2,
        BRapida: 5,
        citologica:6,
        necropsia:0
    },
    {
        id: 3,
        servicio:'Broncopulmonar',
        biopsias: 1,
        BRapida: 3,
        citologica:4,
        necropsia:0

    },
    {
        id: 4,
        servicio:'Consultorio Nogales',
        biopsias: 10,
        BRapida: 15,
        citologica:20,
        necropsia:6

    }]
    const cols = [
        { field: 'id', header: 'Código' },
        { field: 'servicio', header: 'Servicio' },
        { field: 'biopsias', header: 'Biopsias' },
        { field: 'BRapida', header: 'Biopsias Rápidas' },
        { field: 'citologica', header: 'Citologías' },
        { field: 'necropsia', header: 'Necropsias' },
    ];
    const onSelectionChange = (e) => {
        setSelectedProducts(e.value);
    }
    const exportColumns = cols.map(col => ({ title: col.header, dataKey: col.field }));

    useEffect(() => {
        setProducts(biopsias);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    }

    const exportPdf = () => {
        import('jspdf').then(jsPDF => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);
                doc.autoTable(exportColumns, products);
                doc.save('products.pdf');
            })
        })
    }
    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then(module => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });

                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    }
    const exportExcel = () => {
        import('xlsx').then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet(products);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            let date = new Date();
            var newdate = new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(date, 'yyyy-MM-dd');
            const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            saveAsExcelFile(excelBuffer, 'products'+newdate);
        });
    }
    const header = (
        <div className="flex align-items-center export-buttons">
            <Button type="button" icon="pi pi-file" onClick={() => exportCSV(false)} className="mr-2" data-pr-tooltip="CSV" />
            <Button type="button" icon="pi pi-file-excel" onClick={exportExcel} className="p-button-success mr-2" data-pr-tooltip="XLS" />
            <Button type="button" icon="pi pi-file-pdf" onClick={exportPdf} className="p-button-warning mr-2" data-pr-tooltip="PDF" />
            <Button type="button" icon="pi pi-filter" onClick={() => exportCSV(true)} className="p-button-info ml-auto" data-pr-tooltip="Selection Only" />
        </div>
    );
    return (
        <React.Fragment>
            <div className="card  ">
                <h5>Export</h5>
                <Tooltip target=".export-buttons>button" position="bottom" />

                <DataTable ref={dt} value={products} header={header} dataKey="id" responsiveLayout="scroll"
                    selectionMode="multiple" selection={selectedProducts} onSelectionChange={onSelectionChange}>
                    {
                        cols.map((col, index) => <Column key={index} field={col.field} header={col.header} />)
                    }
                </DataTable>
            </div>
        </React.Fragment>
        );
};
export default ServicioEstadisticas;

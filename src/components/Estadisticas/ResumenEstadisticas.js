import React, { useState, useEffect, useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { Chart } from 'primereact/chart';
import { Button } from "primereact/button";
import { BiopsiaByFecha } from "./EstadisticasService";
import Estadisticas from "./Estadisticas";


const ResumenEstadisticas = (props) => {
    //#region Grafico Titales por tipo de examen

useEffect (() => {
search();

},[] )   
const search=()=> {
    console.log(props);
    // console.log(props.resumen1001);
    // console.log(props.resumen1002);
    // console.log(props.resumen1003);
    // console.log(props.resumen1004);
    // console.log(props.resumen1005);
    // console.log(props.resumen1006);
    // console.log(props.resumen1007);
    // console.log(props.resumen1008);
    // console.log(props.resumen1009);
    // console.log(props.resumen1010);

}

    const [PieBiopsias] = useState({
        labels: ['Biopsias', 'Rápidas', 'Citologicas', 'Necropsias'],


        datasets: [
            {
                data: [props.resumenBiop, props.resumenBiopRapida, props.resumenCitolog, props.resumenNecropsia],
                backgroundColor: [
                    "#42A5F5",
                    "#66BB6A",
                    "#FFA726",
                    "#FF5722",
                ],
                hoverBackgroundColor: [
                    "#64B5F6",
                    "#81C784",
                    "#FFB74D",
                    "#FF8022",
                ]
            }
        ]
    });

    const [lightOptionsBiopsias] = useState({
        plugins: {
            legend: {
                labels: {
                    color: '#495057'
                }
            }
        }
    });
    //#endregion
    //#region Grafico totales materiales utilizados
    const [DonaMateriales] = useState({
        labels: ['Moldes', 'He', 'Tincion Recortes', 'Citologicos', 'Histoquímico', 'Inmunohist', 'Placas BR'],
        datasets: [
            {
                data: [20, 50, 30, 10, 60, 45, 80],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#c86e1f",
                    "#6fc70b",
                    "#f1f509",
                    "#033644",

                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#bc8c62",
                    "#9fbf7a",
                    "#f8fa96",
                    "#37575f",
                ]
            }]
    });

    const [lightOptionsMateriales] = useState({
        plugins: {
            legend: {
                labels: {
                    color: '#495057'
                }
            }
        }
    });
    //#endregion
    //#region Grafico total codigos fonasa
    setTimeout(() => {
        


    })
    const [barrasCodigosFonasa] = useState({
        labels: ['801001', '801002', '801003', '801004', '801005', '801006', '801007', '801008', '801009', '801010'],
        datasets: [
            {
                label: 'Codigos Fonasa',
                backgroundColor: '#42A5F5',
                data: [props.sum1001, props.sum1002, props.sum1003, props.sum1004, props.sum1005, props.sum1006, props.sum1007, props.sum1008, props.sum1009, props.sum1010]
            }]
    });
    let basicOptions = {

        maintainAspectRatio: false,
        aspectRatio: .8,
        plugins: {
            legend: {
                labels: {
                    color: '#495057'
                }
            }
        },
        scales: {
            x: {
                ticks: {
                    color: '#495057'
                },
                grid: {
                    color: '#ebedef'
                }
            },
            y: {
                ticks: {
                    color: '#495057'
                },
                grid: {
                    color: '#ebedef'
                }
            }
        }
    };
    //#endregion
    return (
        <React.Fragment>
            <div className="p-grid">
                <div className="p-fluid formgrid grid">
                    <div className="field col-12 md:col-12">
                        <div className="flex card justify-content-center">
                            <div className="field col-12 md:col-3">

                                <Button type="submit" className="mr-2 mb-0" label="Generar Reporte" />

                            </div>
                        </div>

                    </div>
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="flex card justify-content-center">
                                <h5>Totales Por Tipo de Examen</h5>
                            </div>
                            <div className="card flex justify-content-center">

                                <Chart type="pie" data={PieBiopsias} options={lightOptionsBiopsias} style={{ width: '60%' }} />

                            </div>
                        </div>

                    </div>
                    <div className="field col-12 md:col-6">
                        <div className="card">
                            <div className="flex card justify-content-center">
                                <h5>Total de Materiales</h5>
                            </div>
                            <div className="card flex justify-content-center">
                                <Chart type="doughnut" data={DonaMateriales} options={lightOptionsMateriales} style={{ width: '60%' }} />
                            </div>
                        </div>
                    </div>
                    <div className="field col-12 md:col-12">
                        <div className="card">
                            <div className="flex justify-content-center">
                                <h5>Totales por Código Fonasa</h5>
                            </div>
                            <hr />

                            <Chart type="bar" data={barrasCodigosFonasa} options={basicOptions} />

                        </div>
                    </div>
                </div>
            </div>

        </React.Fragment>
    )
};
export default ResumenEstadisticas;

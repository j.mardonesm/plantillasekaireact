import axios from "axios";


export async function BiopsiaByFecha (desde:string, hasta:string) 
{
    try{
        const response = await axios.get(` http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/rangofecha?desde=${desde}&hasta=${hasta}`)
                return response.data;
    }catch (error){
        console.log(error);
    return null;
    }
}



export async function CodFonasa (id:string)
{
    try {
            const response = await axios.get(` http://10.69.206.32:8080/api-apa-codigo-fonasa/api/codfonasa/${id}`)
            return response.data;

    } catch (error) {
    console.log(error);
    return null;        
    }
}

export async function listServicios(){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-servicios/api/servicios`);
        return response.data;
    }  catch (error) {
      console.log(error);
      return null;

    }
}

export async function servicioById(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-core-servicios/api/servicios/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;

    }
}
import React, { useState, useEffect} from "react";
import { Button } from "primereact/button";
import {searchDiagnosticosById, searchMacroMicro } from "./InformeBiopsiaService";
import logoHCSBA from "../../assets/img/logoHCSBA.jpg";

const InformeBiopsia = ({ idBiopsia, Ficha, DataPaciente, BiopsiaBus, edadPaciente, fechasBiopsia }) => {
    const [imagenHCSBA] = useState(logoHCSBA);
    const [InformeBiopsia, setInformeBiopsia] = useState({});
    const [base64, setBase64] = useState();
    useEffect(() => {
        search();
    }, []);
    const InformeModel = {
        nombre: "",
        edad: "",
        rut: "",
        ficha: "",
        fechaTomaMuestra: "",
        fechaRecepcion: "",
        solicitadoPor: "",
        muestra: "",
        antecedentes: "",
        micro: "",
        macro: "",
        diagnostico: "",
        nota: "",
        nBiopsia: "",
        dTecnico: "",
        patologo: "",
        fechaInforme: ""
    };
    const search = async () => {
        let logo
        var img = new Image();
        img.src = imagenHCSBA;
        console.log(img.src);
        toDataURL(img.src, function (dataURL) {
            //alert(dataURL);
            logo = dataURL;
            console.log(logo);
            setBase64(logo);
        });
        //let datPaciente = DataPaciente
        console.log(imagenHCSBA);
        console.log(idBiopsia);
        console.log(Ficha);
        console.log(DataPaciente);
        console.log(BiopsiaBus);
        console.log(fechasBiopsia);
        let macromicro = null;
        try {
            macromicro = await searchMacroMicro(idBiopsia);
        } catch (error) {

        }

        console.log(macromicro);
        let diag = null;
        try {
            diag = await searchDiagnosticosById(idBiopsia);
        } catch (error) {

        }
        console.log(diag);
        let macroscopia = "";
        let microscopia = "";
        let diagnostico = "";
        let nota = "";
        if (macromicro) {
            if (macromicro.macroscopia !== "" && macromicro.macroscopia !== undefined && macromicro.macroscopia !== null) {
                macroscopia = macromicro.macroscopia.replace(/<[^>]+>/g, '');
            }
            if (macromicro.microscopia !== "" && macromicro.microscopia !== undefined && macromicro.microscopia !== null) {
                microscopia = macromicro.microscopia.replace(/<[^>]+>/g, '');
            }
        }
        if (diag) {
            if (diag.diagnostico !== "" && diag.diagnostico !== undefined && diag.diagnostico !== null) {
                diagnostico = diag.diagnostico;
            }
            if (diag.notas !== "" && diag.notas !== undefined && diag.notas !== null) {
                nota = diag.notas;
            }
        }
        console.log(macroscopia);
        console.log(microscopia);
        console.log(diagnostico);
        console.log(nota);
        console.log(BiopsiaBus);
        let FechaRecepMuestra = BiopsiaBus.fecharecepcionmuestra;
        let FechaTomaMuestra = BiopsiaBus.fechatomamuestra;
        let FechaInforme = BiopsiaBus.fechainforme;
        FechaInforme = new Date(FechaInforme.getFullYear() + "-" + (FechaInforme.getMonth() + 1) + "-" + (FechaInforme.getDate()));
        FechaRecepMuestra = new Date(FechaRecepMuestra.getFullYear() + "-" + (FechaRecepMuestra.getMonth() + 1) + "-" + (FechaRecepMuestra.getDate()));
        FechaTomaMuestra = new Date(FechaTomaMuestra.getFullYear() + "-" + (FechaTomaMuestra.getMonth() + 1) + "-" + (FechaTomaMuestra.getDate()));
        let FechaRecepMuestraFormated = FechaRecepMuestra;
        let FechaTomaMuestraFormated = FechaTomaMuestra;
        let FechaInformeFormated = FechaInforme;
        if (BiopsiaBus.fecharecepcionmuestra === undefined) {
            FechaRecepMuestra = "";
        } else {
            FechaRecepMuestra = new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(FechaRecepMuestraFormated);
        }
        if (BiopsiaBus.fechatomamuestra === undefined) {
            FechaTomaMuestra = "";
        } else {
            FechaTomaMuestra = new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(FechaTomaMuestraFormated);
        }
        if (BiopsiaBus.fechainforme === undefined) {
            FechaInforme = "";
        } else {
            FechaInforme = new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(FechaInformeFormated);
        }
        InformeModel.nombre = DataPaciente.primerNombre + " " + DataPaciente.apellidoPaterno + " " + DataPaciente.apellidoMaterno;
        InformeModel.edad = edadPaciente;
        InformeModel.rut = DataPaciente.nroDocumento;
        InformeModel.ficha = Ficha;
        InformeModel.fechaTomaMuestra = FechaTomaMuestra;
        InformeModel.fechaRecepcion = FechaRecepMuestra;
        InformeModel.fechaInforme = FechaInforme;
        InformeModel.solicitadoPor = "Fernando Lopez";
        InformeModel.muestra = BiopsiaBus.muestraenviada;
        InformeModel.antecedentes = BiopsiaBus.antecedentes;
        InformeModel.macro = macroscopia;
        InformeModel.micro = microscopia;
        InformeModel.diagnostico = diagnostico;
        InformeModel.nota = nota;
        InformeModel.nBiopsia = BiopsiaBus.nroBiopsia;
        InformeModel.dTecnico = "Francisco Carpintero";
        InformeModel.patologo = "Roger Salvo";
        setInformeBiopsia({ ...InformeModel });
        console.log(InformeModel);





    };
    function toDataURL(src, callback) {
        var image = new Image();
        image.crossOrigin = 'Anonymous';

        image.onload = function () {
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            canvas.height = this.naturalHeight;
            canvas.width = this.naturalWidth;
            context.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL('image/jpeg');
            callback(dataURL);
        };
        image.src = src;
    }
    const exportPdf = async () => {
        console.log(base64);
        console.log(InformeBiopsia);
        var fontSizes = {
            HeadTitleFontSize: 18,
            Head2TitleFontSize: 16,
            TitleFontSize: 14,
            SubTitleFontSize: 12,
            NormalFontSize: 10,
            SmallFontSize: 8
        };

        var lineSpacing = {
            NormalSpacing: 12,
            Subtitle: 1,
        };
        import('jspdf').then(jsPDF => {
            const doc = new jsPDF.default('p', 'pt', 'letter');
            doc.page = 1; // use this as a counter.
            function footer() {
                doc.text(`${doc.page}`,300, 780 ); //print number bottom right
                doc.page++;
            };
            console.log(doc.getFontList());
            const docWidth = doc.internal.pageSize.getWidth();
            const docHeight = doc.internal.pageSize.getHeight();
            console.log(docHeight);
            var startX = 40;
            var startY = 100;
            var encabezado = 'Servicio de Salud Metropolitano Central\n Complejo de Salud San Borja Arriarán\n      Unidad de Anatomía Patológica';
            doc.setFontSize(fontSizes.HeadTitleFontSize);
            const splitDescription = doc.splitTextToSize(
                encabezado,
                docWidth - 20
            );
            doc.text(splitDescription, 145, 40);
            doc.setFontSize(fontSizes.NormalFontSize);
            doc.text(`Fecha Informe ${InformeBiopsia.fechaInforme}`, 470, 95);
            doc.addImage(base64, "jpg", 25, 15, 80, 80);
            doc.line(0, 100, docWidth, 100);
            doc.setFont("helvetica", "italic", "bold");
            doc.setFontSize(fontSizes.NormalFontSize);
            doc.text(`NOMBRE`, 35, startY += lineSpacing.NormalSpacing + 15);
            doc.text(`Biopsia N°: ${InformeBiopsia.nBiopsia}`, 450, startY - 15);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.nombre}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`EDAD`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.edad}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`RUT`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.rut}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`Nro.FICHA`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.ficha}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`TOMA DE MUESTRA`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.fechaTomaMuestra}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`RECEPCION DE MUESTRA`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.fechaRecepcion}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`SOLICITADO POR`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.solicitadoPor}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`MUESTRA DE`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            doc.text(`: ${InformeBiopsia.muestra}`, 180, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`ANTECEDENTES`, 35, startY += lineSpacing.NormalSpacing);
            doc.setFont("helvetica", "normal");
            footer()
            doc.text(`: ${InformeBiopsia.antecedentes}`, 180, startY, { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
            doc.setFontSize(fontSizes.Head2TitleFontSize);
            doc.setFont("helvetica", "sans-serif", "bold");
            doc.text("INFORME ANATOMO PATOLÓGICO", 170, startY += lineSpacing.NormalSpacing + 15);
            doc.line(170, startY += lineSpacing.Subtitle, 434, startY);
            doc.setFont("helvetica", "italic", "bold");
            doc.setFontSize(fontSizes.NormalFontSize);
            var microscopia = (`${InformeBiopsia.micro}`);
            const loremSlit = doc.splitTextToSize(
                microscopia,
                docWidth - 100,
            );
            console.log(loremSlit);
            var macroscopia = (`${InformeBiopsia.macro}`);
            const loremSlit2 = doc.splitTextToSize(
                macroscopia,
                docWidth - 100,
            );
            console.log(loremSlit2);
            var lor = {
                micro: "EXÁMEN MICROSCÓPICO: ",
                lor1: loremSlit,
                macro: "EXÁMEN MACROSCÓPICO:",
                lor2: loremSlit2

            };
            let dimMicro = doc.getTextDimensions(lor.lor1);
            console.log(dimMicro.h);
            console.log(startY + dimMicro.h);
            console.log(docHeight);
            if ((startY + dimMicro.h) > docHeight - 100) {
                doc.addPage("letter");
                startY = 0;
                footer();
            }
            if (InformeBiopsia.micro !== "" && InformeBiopsia.micro !== undefined && InformeBiopsia.micro !== null) {
                doc.text(lor.micro, startX - 10, startY += lineSpacing.NormalSpacing + 20);
                doc.setFont("helvetica", "normal");
                doc.text(lor.lor1, startX, startY += lineSpacing.NormalSpacing * 1.01, { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
            }
            if ((startY + dimMicro.h) >= docHeight - 300) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            console.log(startY + dimMicro.h);
            console.log(startY);
            console.log(docHeight);
            console.log(startY > docHeight - 300);

            doc.setFont("helvetica", "italic", "bold");
            let dimMacro = doc.getTextDimensions(lor.lor2);
            if (startY === 0) {
                if (InformeBiopsia.macro !== "" && InformeBiopsia.macro !== undefined && InformeBiopsia.macro !== null) {
                    doc.text(lor.macro, startX - 10, startY += lineSpacing.NormalSpacing + 30);
                    doc.setFont("helvetica", "normal");
                    doc.text(lor.lor2, startX, startY += lineSpacing.NormalSpacing, { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
                }
            } else {
                if ((startY + dimMacro.h) > docHeight - 200) {
                    doc.addPage("letter");
                    startY = 0;
                    if (InformeBiopsia.macro !== "" && InformeBiopsia.macro !== undefined && InformeBiopsia.macro !== null) {
                        doc.text(lor.macro, startX - 10, startY += lineSpacing.NormalSpacing + 30);
                        doc.setFont("helvetica", "normal");
                        doc.text(lor.lor2, startX, startY += lineSpacing.NormalSpacing, { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
                    }
                } else {
                    if (InformeBiopsia.macro !== "" && InformeBiopsia.macro !== undefined && InformeBiopsia.macro !== null) {
                        doc.text(lor.macro, startX - 10, startY += lineSpacing.NormalSpacing + dimMicro.h + 30);
                        doc.setFont("helvetica", "normal");
                        doc.text(lor.lor2, startX, startY += lineSpacing.NormalSpacing, { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
                    }
                }

            }

            if ((startY + dimMacro.h) >= docHeight - 300) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            doc.setFont("helvetica", "italic", "bold");
            var diagnostico = (`${InformeBiopsia.diagnostico}`);
            const diagnosticoSplit = doc.splitTextToSize(
                diagnostico,
                docWidth - 100,
            );
            let diagnosticoDim = doc.getTextDimensions(diagnosticoSplit);
            if ((startY + diagnosticoDim.h) > docHeight - 200) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            doc.text("DIAGNÓSTICO: ", startX - 10, (startY += lineSpacing.NormalSpacing + 80));
            doc.setFont("helvetica", "normal");

            doc.text(`${diagnosticoSplit}`, startX, (startY += lineSpacing.NormalSpacing), { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
            doc.setFont("helvetica", "italic", "bold");
            if (startY > docHeight) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            var nota = (`${InformeBiopsia.nota}`);
            const notaSplit = doc.splitTextToSize(
                nota,
                docWidth - 100,
            );
            let notaDim = doc.getTextDimensions(notaSplit);
            if ((startY + notaDim.h) > docHeight - 200) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            if (InformeBiopsia.nota !== "" && InformeBiopsia.nota !== undefined && InformeBiopsia.nota !== null) {
                doc.text("NOTA: ", startX - 10, startY += lineSpacing.NormalSpacing +diagnosticoDim.h + 30);
                doc.setFont("helvetica", "normal");
                doc.text(`${notaSplit}`, startX, (startY += lineSpacing.NormalSpacing), { align: 'justify', lineHeightFactor: 1.5, maxWidth: 520 });
            }
            if (startY >= docHeight - 200) {
                doc.addPage("letter");
                startY = 0;
                console.log("nueva Pagina");
                footer();
            }
            doc.setFont("helvetica", "italic", "bold");
            doc.text(`${InformeBiopsia.dTecnico}`, startX + 50, startY += lineSpacing.NormalSpacing + 100);
            doc.text(`${InformeBiopsia.patologo}`, startX + 360, startY);
            doc.text("Director Técnico", startX + 50, startY += lineSpacing.NormalSpacing);
            doc.text("Anátomo Patológo", startX + 360, startY);
            //doc.setFont('italic');
            //doc.text(DatosPaciente.Nombre, 80, startY);
            //(method) jsPDF.output(type: "dataurlnewwindow" | "pdfobjectnewwindow" | "pdfjsnewwindow", options?: {
            doc.output("pdfobjectnewwindow", { filename: 'pdfTest' });
            //doc.save('InvoiceTemplate.pdf');

        })

    }
    return (
        <React.Fragment>
            <div className="p-grid">
                <div className="col-12">
                    <div className="p-fluid formgrid grid">
                        <Button type="button" icon="pi pi-file-pdf" className="p-button-warning mr-2" data-pr-tooltip="PDF" onClick={exportPdf} />
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}
export default InformeBiopsia;

import axios from 'axios';

export async function searchBiopsiaByid(id: string) {
    try {
        const response  = await axios.get(`http://10.69.206.32:8080/api-apa-biopsia/api/biopsia/id/${id}`);
        return response.data;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export async function searchMacroMicro(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-macro-micro/api/macromicro/${id}`);
        return response.data;

    } catch (error) {
        console.log(error);
        return null;

    }
}
export async function searchDiagnosticosById(id: string){
    try {
        const response = await axios.get(`http://10.69.206.32:8080/api-apa-diagnostico/api/diagnostico/biopsia/${id}`);

        return response.data;
      } catch (err) {
        console.error(err);
        return null;
      }
}
